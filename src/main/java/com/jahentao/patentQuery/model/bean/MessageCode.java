package com.jahentao.patentQuery.model.bean;

/**
 * 前后台要约定好消息码
 * @author jahentao
 * @date 2018/4/30
 * @since 1.0
 */
public class MessageCode {

    // ======== 系统消息码从1开始============

    /**系统正常*/
    public static final String SUCCESS = "001";
    /**系统不正常*/
    public static final String ABNORMAL = "002";

    //========= 应用消息码从1开始============
    /**专利名称为空*/
    public static final String PATENT_NAME_NULL = "101";
    /**专利号为空*/
    public static final String PATENT_ID_NULL = "102";
    /**搜索关键词为空*/
    public static final String QUERY_KEYWORD_NULL = "103";
    /**专利号不正确*/
    public static final String INCORRECT_PATENT_ID = "104";
    /**专利类型不正确*/
    public static final String INCORRECT_PATENT_TYPE = "105";
    /**专利状态不正确*/
    public static final String INCORRECT_PATENT_STATUS = "106";
    /**删除失败*/
    public static final String DELETE_FAILURE = "107";
    /**该专利号数据库已存在*/
    public static final String PATENT_ID_ALREADY_EXISTS = "108";
    /**保存失败*/
    public static final String SAVE_FAILURE = "109";
    /**上传成功*/
    public static final String UPLOAD_SUCCESS = "110";
    /**上传失败*/
    public static final String UPLOAD_FAILURE = "111";

    //========= 用户登录、注册消息码============
    /**没有操作权限，请登录系统*/
    public static final String PLEASE_LOGIN = "201";
    /**系统没有该用户*/
    public static final String SYS_NO_USER = "202";
    /**用户名或密码错误*/
    public static final String SYS_NO_USER_AND_PASSWORD = "203";
    /**请输入验证码*/
    public static final String PLASS_CAPTCHA = "204";
    /**账户错误次数过多,暂时禁止登录!*/
    public static final String SYS_LOG_IN_TOO_MANY = "205";
    /**验证码不正确*/
    public static final String CAPTCHA_ERROR = "206";
    /**请注册帐号*/
    public static final String PLEASE_REGISTER = "301";
    /**jcaptcha 验证码为空*/
    public static final String JCAPTCHA_EMPTY = "302";
    /**jcaptcha验证码错误*/
    public static final String JCAPTCHA_ERROR = "303";
    /**注册验证码邮件发送失败*/
    public static final String CAPTCHA_EMAIL_SEND_FAILURE = "304";
    /**注册短信验证码发送失败*/
    public static final String CAPTCHA_SMS_SEND_FAILURE = "305";
    /**后台对用户填写的验证码验证，结果不正确；或已过期*/
    public static final String CAPTCHA_VERIFICATION_FAILURE = "306";
    /**用户注册失败*/
    public static final String REGISTER_FAILURE = "307";
    /**用户已存在*/
    public static final String REGISTER_USER_EXIST = "308";
    /**会话已过期*/
    public static final String INVALID_SESSION = "309";
    /**用户退出登录*/
    public static final String USER_LOGOUT = "310";
    /**QQ用户OpenId不合法*/
    public static final String OPENID_INVALID = "311";
    /**QQ用户OpenId合法*/
    public static final String OPENID_VALID = "312";

}

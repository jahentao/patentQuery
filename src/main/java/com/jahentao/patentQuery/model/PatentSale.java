package com.jahentao.patentQuery.model;

import java.io.Serializable;
import java.util.Date;

public class PatentSale implements Serializable {
    private Long id;

    private PatentSupply supply;

    private PatentDemand demand;

    private Date gmtCreate;

    private Date gmtModified;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PatentSupply getSupply() {
        return supply;
    }

    public void setSupply(PatentSupply supply) {
        this.supply = supply;
    }

    public PatentDemand getDemand() {
        return demand;
    }

    public void setDemand(PatentDemand demand) {
        this.demand = demand;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PatentSale other = (PatentSale) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getSupply().getId() == null ? other.getSupply().getId() == null : this.getSupply().getId().equals(other.getSupply().getId()))
            && (this.getDemand().getId() == null ? other.getDemand().getId() == null : this.getDemand().getId().equals(other.getDemand().getId()))
            && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getSupply().getId() == null) ? 0 : getSupply().getId().hashCode());
        result = prime * result + ((getDemand().getId() == null) ? 0 : getDemand().getId().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "PatentSale{" +
                "id=" + id +
                ", supply=" + supply +
                ", demand=" + demand +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
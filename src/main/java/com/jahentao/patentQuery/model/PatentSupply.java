package com.jahentao.patentQuery.model;

import java.io.Serializable;
import java.util.Date;

public class PatentSupply implements Serializable {
    private Long id;

    private User user;

    private Patent patent;

    private Date supplyTime;

    private Date gmtCreate;

    private Date gmtModified;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Patent getPatent() {
        return patent;
    }

    public void setPatent(Patent patent) {
        this.patent = patent;
    }

    public Date getSupplyTime() {
        return supplyTime;
    }

    public void setSupplyTime(Date supplyTime) {
        this.supplyTime = supplyTime;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PatentSupply other = (PatentSupply) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getUser().getId() == null ? other.getUser().getId() == null : this.getUser().getId().equals(other.getUser().getId()))
            && (this.getPatent() == null ? other.getPatent() == null : this.getPatent().equals(other.getPatent()))
            && (this.getSupplyTime() == null ? other.getSupplyTime() == null : this.getSupplyTime().equals(other.getSupplyTime()))
            && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUser().getId() == null) ? 0 : getUser().getId().hashCode());
        result = prime * result + ((getPatent().getId() == null) ? 0 : getPatent().getId().hashCode());
        result = prime * result + ((getSupplyTime() == null) ? 0 : getSupplyTime().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "PatentSupply{" +
                "id=" + id +
                ", user=" + user +
                ", patent='" + patent + '\'' +
                ", supplyTime=" + supplyTime +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
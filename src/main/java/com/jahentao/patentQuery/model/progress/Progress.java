package com.jahentao.patentQuery.model.progress;

import java.io.Serializable;

/**
 * 需要对任务进度监控，所以抽象这个过程类
 * <p>适用的任务，比如：文件上传、批量处理、委托转换等</p>
 * <p>对过程记录在缓存中，当然，Cache只是简单的加速，而不能保证数据的安全。</p>
 * @author jahentao
 * @date 2018/5/31
 * @since 1.0
 */
public class Progress implements Serializable {

    /** 总量。e.g. 对文件上传类任务来说，就是文件总大小，如字节数；对不那么具体的任务需要抽象出步骤，这就是总步骤数。
     * 根据进展环节，也可能出现，抽象的总步骤数增加或减少的情况。*/
    private Integer total;
    /** 完成量。e.g. 对文件上传类任务来说，就是已传输写入的字节数；对不那么具体的任务来说，就是已完成的步骤数。*/
    private Integer completed;
    /** 进度，百分比，范围[0,100]。完成量占总量的百分比。*/
    private Float percentage;
    public static final String COMPLETE_PERCENTAGE = "100";

    public Progress() {
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    /**
     * 每次获取进度调用
     * @return
     */
    public Float getPercentage() {
        if (percentage.equals(100F))  {
            return 100F;
        }

        percentage = completed / (float) total * 100;
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }
}

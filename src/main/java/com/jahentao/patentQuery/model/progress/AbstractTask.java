package com.jahentao.patentQuery.model.progress;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 对任务的抽象
 * <p>可扩展因素：对任务拆解多线程处理、定期定时任务等</p>
 * <p>主要的实现，由于需要操纵数据库，所以设计上采取在Controller中新建匿名内部类的方式</p>
 * TODO 其实从Excel批量导入，之前实现的业务，也是任务。有时间再整合到一起吧
 * @author jahentao
 * @date 2018/6/1
 * @since 1.0
 */
public abstract class AbstractTask {

    /**
     * 任务的id，也是放入缓存中，记录任务进度的key
     */
    private Long id;
    private String taskName;
    private Progress progress;

    public AbstractTask(Long id) {
        super();
        this.id = id;
        this.taskName = "task-" + id;
        this.progress = new Progress();
    }

    /**
     * 抽象方法，子类需实现该方法，指定具体的任务
     */
    public abstract void execute();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Progress getProgress() {
        return progress;
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }
}

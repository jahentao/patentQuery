package com.jahentao.patentQuery.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jahentao
 */
public class Patent implements Serializable {
    private String id;

    private String name;

    private Byte type;

    /**
     * 专利类型
     */
    public enum PatentTypeEnum {

        /**
         * 发明类型专利
         */
        INVENTION_PATENT("发明", (byte)1),
        /**
         * 实用新型类型专利
         */
        UTILITY_MODEL_PATENT("实用新型", (byte)2),
        /**
         * 外观设计类型专利
         */
        DESIGN_PATENT("外观设计", (byte)3);

        /**
         * 类型
         */
        private String type;
        /**
         * 编号
         */
        private Byte index;

        /**
         * 构造方法只能私有
         */
        private PatentTypeEnum(String type, Byte index) {
            this.type = type;
            this.index = index;
        }

        public static String getName(int index) {
            for (PatentTypeEnum typeEnum : PatentTypeEnum.values()) {
                if (typeEnum.index == index) {
                    return typeEnum.type;
                }
            }
            return null;
        }

        public static Byte getIndex(String type) {
            for (PatentTypeEnum typeEnum : PatentTypeEnum.values()) {
                if (typeEnum.type.equals(type)) {
                    return typeEnum.index;
                }
            }
            return null;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(Byte index) {
            this.index = index;
        }

        @Override
        public String
        toString() {
            return "PatentTypeEnum{" +
                    "type='" + type + '\'' +
                    ", index=" + index +
                    '}';
        }
    }

    private String applicationNum;

    private Date applicationDate;

    private String publicationNum;

    private Date publicationDate;

    private String ipc;

    private String applicants;

    private String applicantsInstitute;

    private String inventors;

    private String priorityNum;

    private String priorityDate;

    private String agents;

    private String abstractInfo;

    private String pdfAddress;

    private Byte lawStatus;

    /**
     * 专利状态
     */
    public enum PatentStatusEnum {

        /**
         * 未知
         */
        UNKNOWN("未知", (byte)0),
        /**
         * 授权未缴费
         */
        UNPAID("未缴费", (byte)1),
        /**
         * 授权已下证
         */
        CERTIFICATE_ISSUED("已下证", (byte)2);

        private String status;
        private Byte index;

        private PatentStatusEnum(String status, Byte index) {
            this.status = status;
            this.index = index;
        }

        public static String getStatus(int index) {
            for (PatentStatusEnum status : PatentStatusEnum.values()) {
                if (status.index == index) {
                    return status.status;
                }
            }
            return null;
        }

        public static Byte getIndex(String status) {
            for (PatentStatusEnum patentStatus : PatentStatusEnum.values()) {
                if (patentStatus.status.equals(status)) {
                    return patentStatus.index;
                }
            }
            return null;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(Byte index) {
            this.index = index;
        }

        @Override
        public String toString() {
            return "PatentStatus{" +
                    "status='" + status + '\'' +
                    ", index=" + index +
                    '}';
        }
    }

    private String logo;

    private Date gmtCreate;

    private Date gmtModified;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 根据专利号，设置专利状态
     * <p>依据是专利号第五位数：
     * <ul>
     *     <li>“1”代表  发明专利</li>
     *     <li>“2”代表  实用新型专利</li>
     *     <li>“3”代表  外观设计专利</li>
     * </ul>
     * </p>
     */
    public void setType(String id) {
        switch (id.substring(4,5)) {
            case "1" : this.type = (byte)1; break;
            case "2" : this.type = (byte)2; break;
            case "3" : this.type = (byte)3; break;
            default: break;
        }
    }

    public String getApplicationNum() {
        return applicationNum;
    }

    public void setApplicationNum(String applicationNum) {
        this.applicationNum = applicationNum;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getPublicationNum() {
        return publicationNum;
    }

    public void setPublicationNum(String publicationNum) {
        this.publicationNum = publicationNum;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getIpc() {
        return ipc;
    }

    public void setIpc(String ipc) {
        this.ipc = ipc;
    }

    public String getApplicants() {
        return applicants;
    }

    public void setApplicants(String applicants) {
        this.applicants = applicants;
    }

    public String getApplicantsInstitute() {
        return applicantsInstitute;
    }

    public void setApplicantsInstitute(String applicantsInstitute) {
        this.applicantsInstitute = applicantsInstitute;
    }

    public String getInventors() {
        return inventors;
    }

    public void setInventors(String inventors) {
        this.inventors = inventors;
    }

    public String getPriorityNum() {
        return priorityNum;
    }

    public void setPriorityNum(String priorityNum) {
        this.priorityNum = priorityNum;
    }

    public String getPriorityDate() {
        return priorityDate;
    }

    public void setPriorityDate(String priorityDate) {
        this.priorityDate = priorityDate;
    }

    public String getAgents() {
        return agents;
    }

    public void setAgents(String agents) {
        this.agents = agents;
    }

    public String getAbstractInfo() {
        return abstractInfo;
    }

    public void setAbstractInfo(String abstractInfo) {
        this.abstractInfo = abstractInfo;
    }

    public String getPdfAddress() {
        return pdfAddress;
    }

    public void setPdfAddress(String pdfAddress) {
        this.pdfAddress = pdfAddress;
    }

    public Byte getLawStatus() {
        return lawStatus;
    }

    public void setLawStatus(Byte lawStatus) {
        this.lawStatus = lawStatus;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Patent other = (Patent) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getApplicationNum() == null ? other.getApplicationNum() == null : this.getApplicationNum().equals(other.getApplicationNum()))
            && (this.getApplicationDate() == null ? other.getApplicationDate() == null : this.getApplicationDate().equals(other.getApplicationDate()))
            && (this.getPublicationNum() == null ? other.getPublicationNum() == null : this.getPublicationNum().equals(other.getPublicationNum()))
            && (this.getPublicationDate() == null ? other.getPublicationDate() == null : this.getPublicationDate().equals(other.getPublicationDate()))
            && (this.getIpc() == null ? other.getIpc() == null : this.getIpc().equals(other.getIpc()))
            && (this.getApplicants() == null ? other.getApplicants() == null : this.getApplicants().equals(other.getApplicants()))
            && (this.getApplicantsInstitute() == null ? other.getApplicantsInstitute() == null : this.getApplicantsInstitute().equals(other.getApplicantsInstitute()))
            && (this.getInventors() == null ? other.getInventors() == null : this.getInventors().equals(other.getInventors()))
            && (this.getPriorityNum() == null ? other.getPriorityNum() == null : this.getPriorityNum().equals(other.getPriorityNum()))
            && (this.getPriorityDate() == null ? other.getPriorityDate() == null : this.getPriorityDate().equals(other.getPriorityDate()))
            && (this.getAgents() == null ? other.getAgents() == null : this.getAgents().equals(other.getAgents()))
            && (this.getAbstractInfo() == null ? other.getAbstractInfo() == null : this.getAbstractInfo().equals(other.getAbstractInfo()))
            && (this.getPdfAddress() == null ? other.getPdfAddress() == null : this.getPdfAddress().equals(other.getPdfAddress()))
            && (this.getLawStatus() == null ? other.getLawStatus() == null : this.getLawStatus().equals(other.getLawStatus()))
            && (this.getLogo() == null ? other.getLogo() == null : this.getLogo().equals(other.getLogo()))
            && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getApplicationNum() == null) ? 0 : getApplicationNum().hashCode());
        result = prime * result + ((getApplicationDate() == null) ? 0 : getApplicationDate().hashCode());
        result = prime * result + ((getPublicationNum() == null) ? 0 : getPublicationNum().hashCode());
        result = prime * result + ((getPublicationDate() == null) ? 0 : getPublicationDate().hashCode());
        result = prime * result + ((getIpc() == null) ? 0 : getIpc().hashCode());
        result = prime * result + ((getApplicants() == null) ? 0 : getApplicants().hashCode());
        result = prime * result + ((getApplicantsInstitute() == null) ? 0 : getApplicantsInstitute().hashCode());
        result = prime * result + ((getInventors() == null) ? 0 : getInventors().hashCode());
        result = prime * result + ((getPriorityNum() == null) ? 0 : getPriorityNum().hashCode());
        result = prime * result + ((getPriorityDate() == null) ? 0 : getPriorityDate().hashCode());
        result = prime * result + ((getAgents() == null) ? 0 : getAgents().hashCode());
        result = prime * result + ((getAbstractInfo() == null) ? 0 : getAbstractInfo().hashCode());
        result = prime * result + ((getPdfAddress() == null) ? 0 : getPdfAddress().hashCode());
        result = prime * result + ((getLawStatus() == null) ? 0 : getLawStatus().hashCode());
        result = prime * result + ((getLogo() == null) ? 0 : getLogo().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Patent{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", applicationNum='" + applicationNum + '\'' +
                ", applicationDate=" + applicationDate +
                ", publicationNum='" + publicationNum + '\'' +
                ", publicationDate=" + publicationDate +
                ", ipc='" + ipc + '\'' +
                ", applicants='" + applicants + '\'' +
                ", applicantsInstitute='" + applicantsInstitute + '\'' +
                ", inventors='" + inventors + '\'' +
                ", priorityNum='" + priorityNum + '\'' +
                ", priorityDate='" + priorityDate + '\'' +
                ", agents='" + agents + '\'' +
                ", abstractInfo='" + abstractInfo + '\'' +
                ", pdfAddress='" + pdfAddress + '\'' +
                ", lawStatus=" + lawStatus +
                ", logo='" + logo + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                '}';
    }
}
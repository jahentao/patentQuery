package com.jahentao.patentQuery.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    // =================新增=================
    private String start;// 筛选开始时间

    private String end;// 筛选结束时间

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
    // =================新增=================

    public PatentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Byte value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Byte value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Byte value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Byte value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Byte value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Byte> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Byte> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Byte value1, Byte value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andApplicationNumIsNull() {
            addCriterion("application_num is null");
            return (Criteria) this;
        }

        public Criteria andApplicationNumIsNotNull() {
            addCriterion("application_num is not null");
            return (Criteria) this;
        }

        public Criteria andApplicationNumEqualTo(String value) {
            addCriterion("application_num =", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumNotEqualTo(String value) {
            addCriterion("application_num <>", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumGreaterThan(String value) {
            addCriterion("application_num >", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumGreaterThanOrEqualTo(String value) {
            addCriterion("application_num >=", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumLessThan(String value) {
            addCriterion("application_num <", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumLessThanOrEqualTo(String value) {
            addCriterion("application_num <=", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumLike(String value) {
            addCriterion("application_num like", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumNotLike(String value) {
            addCriterion("application_num not like", value, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumIn(List<String> values) {
            addCriterion("application_num in", values, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumNotIn(List<String> values) {
            addCriterion("application_num not in", values, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumBetween(String value1, String value2) {
            addCriterion("application_num between", value1, value2, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationNumNotBetween(String value1, String value2) {
            addCriterion("application_num not between", value1, value2, "applicationNum");
            return (Criteria) this;
        }

        public Criteria andApplicationDateIsNull() {
            addCriterion("application_date is null");
            return (Criteria) this;
        }

        public Criteria andApplicationDateIsNotNull() {
            addCriterion("application_date is not null");
            return (Criteria) this;
        }

        public Criteria andApplicationDateEqualTo(Date value) {
            addCriterion("application_date =", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateNotEqualTo(Date value) {
            addCriterion("application_date <>", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateGreaterThan(Date value) {
            addCriterion("application_date >", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateGreaterThanOrEqualTo(Date value) {
            addCriterion("application_date >=", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateLessThan(Date value) {
            addCriterion("application_date <", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateLessThanOrEqualTo(Date value) {
            addCriterion("application_date <=", value, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateIn(List<Date> values) {
            addCriterion("application_date in", values, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateNotIn(List<Date> values) {
            addCriterion("application_date not in", values, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateBetween(Date value1, Date value2) {
            addCriterion("application_date between", value1, value2, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andApplicationDateNotBetween(Date value1, Date value2) {
            addCriterion("application_date not between", value1, value2, "applicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationNumIsNull() {
            addCriterion("publication_num is null");
            return (Criteria) this;
        }

        public Criteria andPublicationNumIsNotNull() {
            addCriterion("publication_num is not null");
            return (Criteria) this;
        }

        public Criteria andPublicationNumEqualTo(String value) {
            addCriterion("publication_num =", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumNotEqualTo(String value) {
            addCriterion("publication_num <>", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumGreaterThan(String value) {
            addCriterion("publication_num >", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumGreaterThanOrEqualTo(String value) {
            addCriterion("publication_num >=", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumLessThan(String value) {
            addCriterion("publication_num <", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumLessThanOrEqualTo(String value) {
            addCriterion("publication_num <=", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumLike(String value) {
            addCriterion("publication_num like", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumNotLike(String value) {
            addCriterion("publication_num not like", value, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumIn(List<String> values) {
            addCriterion("publication_num in", values, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumNotIn(List<String> values) {
            addCriterion("publication_num not in", values, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumBetween(String value1, String value2) {
            addCriterion("publication_num between", value1, value2, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationNumNotBetween(String value1, String value2) {
            addCriterion("publication_num not between", value1, value2, "publicationNum");
            return (Criteria) this;
        }

        public Criteria andPublicationDateIsNull() {
            addCriterion("publication_date is null");
            return (Criteria) this;
        }

        public Criteria andPublicationDateIsNotNull() {
            addCriterion("publication_date is not null");
            return (Criteria) this;
        }

        public Criteria andPublicationDateEqualTo(Date value) {
            addCriterion("publication_date =", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateNotEqualTo(Date value) {
            addCriterion("publication_date <>", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateGreaterThan(Date value) {
            addCriterion("publication_date >", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateGreaterThanOrEqualTo(Date value) {
            addCriterion("publication_date >=", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateLessThan(Date value) {
            addCriterion("publication_date <", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateLessThanOrEqualTo(Date value) {
            addCriterion("publication_date <=", value, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateIn(List<Date> values) {
            addCriterion("publication_date in", values, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateNotIn(List<Date> values) {
            addCriterion("publication_date not in", values, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateBetween(Date value1, Date value2) {
            addCriterion("publication_date between", value1, value2, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andPublicationDateNotBetween(Date value1, Date value2) {
            addCriterion("publication_date not between", value1, value2, "publicationDate");
            return (Criteria) this;
        }

        public Criteria andIpcIsNull() {
            addCriterion("ipc is null");
            return (Criteria) this;
        }

        public Criteria andIpcIsNotNull() {
            addCriterion("ipc is not null");
            return (Criteria) this;
        }

        public Criteria andIpcEqualTo(String value) {
            addCriterion("ipc =", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcNotEqualTo(String value) {
            addCriterion("ipc <>", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcGreaterThan(String value) {
            addCriterion("ipc >", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcGreaterThanOrEqualTo(String value) {
            addCriterion("ipc >=", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcLessThan(String value) {
            addCriterion("ipc <", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcLessThanOrEqualTo(String value) {
            addCriterion("ipc <=", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcLike(String value) {
            addCriterion("ipc like", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcNotLike(String value) {
            addCriterion("ipc not like", value, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcIn(List<String> values) {
            addCriterion("ipc in", values, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcNotIn(List<String> values) {
            addCriterion("ipc not in", values, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcBetween(String value1, String value2) {
            addCriterion("ipc between", value1, value2, "ipc");
            return (Criteria) this;
        }

        public Criteria andIpcNotBetween(String value1, String value2) {
            addCriterion("ipc not between", value1, value2, "ipc");
            return (Criteria) this;
        }

        public Criteria andApplicantsIsNull() {
            addCriterion("applicants is null");
            return (Criteria) this;
        }

        public Criteria andApplicantsIsNotNull() {
            addCriterion("applicants is not null");
            return (Criteria) this;
        }

        public Criteria andApplicantsEqualTo(String value) {
            addCriterion("applicants =", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsNotEqualTo(String value) {
            addCriterion("applicants <>", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsGreaterThan(String value) {
            addCriterion("applicants >", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsGreaterThanOrEqualTo(String value) {
            addCriterion("applicants >=", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsLessThan(String value) {
            addCriterion("applicants <", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsLessThanOrEqualTo(String value) {
            addCriterion("applicants <=", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsLike(String value) {
            addCriterion("applicants like", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsNotLike(String value) {
            addCriterion("applicants not like", value, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsIn(List<String> values) {
            addCriterion("applicants in", values, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsNotIn(List<String> values) {
            addCriterion("applicants not in", values, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsBetween(String value1, String value2) {
            addCriterion("applicants between", value1, value2, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsNotBetween(String value1, String value2) {
            addCriterion("applicants not between", value1, value2, "applicants");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteIsNull() {
            addCriterion("applicants_institute is null");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteIsNotNull() {
            addCriterion("applicants_institute is not null");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteEqualTo(String value) {
            addCriterion("applicants_institute =", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteNotEqualTo(String value) {
            addCriterion("applicants_institute <>", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteGreaterThan(String value) {
            addCriterion("applicants_institute >", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteGreaterThanOrEqualTo(String value) {
            addCriterion("applicants_institute >=", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteLessThan(String value) {
            addCriterion("applicants_institute <", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteLessThanOrEqualTo(String value) {
            addCriterion("applicants_institute <=", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteLike(String value) {
            addCriterion("applicants_institute like", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteNotLike(String value) {
            addCriterion("applicants_institute not like", value, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteIn(List<String> values) {
            addCriterion("applicants_institute in", values, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteNotIn(List<String> values) {
            addCriterion("applicants_institute not in", values, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteBetween(String value1, String value2) {
            addCriterion("applicants_institute between", value1, value2, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andApplicantsInstituteNotBetween(String value1, String value2) {
            addCriterion("applicants_institute not between", value1, value2, "applicantsInstitute");
            return (Criteria) this;
        }

        public Criteria andInventorsIsNull() {
            addCriterion("inventors is null");
            return (Criteria) this;
        }

        public Criteria andInventorsIsNotNull() {
            addCriterion("inventors is not null");
            return (Criteria) this;
        }

        public Criteria andInventorsEqualTo(String value) {
            addCriterion("inventors =", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsNotEqualTo(String value) {
            addCriterion("inventors <>", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsGreaterThan(String value) {
            addCriterion("inventors >", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsGreaterThanOrEqualTo(String value) {
            addCriterion("inventors >=", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsLessThan(String value) {
            addCriterion("inventors <", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsLessThanOrEqualTo(String value) {
            addCriterion("inventors <=", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsLike(String value) {
            addCriterion("inventors like", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsNotLike(String value) {
            addCriterion("inventors not like", value, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsIn(List<String> values) {
            addCriterion("inventors in", values, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsNotIn(List<String> values) {
            addCriterion("inventors not in", values, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsBetween(String value1, String value2) {
            addCriterion("inventors between", value1, value2, "inventors");
            return (Criteria) this;
        }

        public Criteria andInventorsNotBetween(String value1, String value2) {
            addCriterion("inventors not between", value1, value2, "inventors");
            return (Criteria) this;
        }

        public Criteria andPriorityNumIsNull() {
            addCriterion("priority_num is null");
            return (Criteria) this;
        }

        public Criteria andPriorityNumIsNotNull() {
            addCriterion("priority_num is not null");
            return (Criteria) this;
        }

        public Criteria andPriorityNumEqualTo(String value) {
            addCriterion("priority_num =", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumNotEqualTo(String value) {
            addCriterion("priority_num <>", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumGreaterThan(String value) {
            addCriterion("priority_num >", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumGreaterThanOrEqualTo(String value) {
            addCriterion("priority_num >=", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumLessThan(String value) {
            addCriterion("priority_num <", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumLessThanOrEqualTo(String value) {
            addCriterion("priority_num <=", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumLike(String value) {
            addCriterion("priority_num like", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumNotLike(String value) {
            addCriterion("priority_num not like", value, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumIn(List<String> values) {
            addCriterion("priority_num in", values, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumNotIn(List<String> values) {
            addCriterion("priority_num not in", values, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumBetween(String value1, String value2) {
            addCriterion("priority_num between", value1, value2, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityNumNotBetween(String value1, String value2) {
            addCriterion("priority_num not between", value1, value2, "priorityNum");
            return (Criteria) this;
        }

        public Criteria andPriorityDateIsNull() {
            addCriterion("priority_date is null");
            return (Criteria) this;
        }

        public Criteria andPriorityDateIsNotNull() {
            addCriterion("priority_date is not null");
            return (Criteria) this;
        }

        public Criteria andPriorityDateEqualTo(String value) {
            addCriterion("priority_date =", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateNotEqualTo(String value) {
            addCriterion("priority_date <>", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateGreaterThan(String value) {
            addCriterion("priority_date >", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateGreaterThanOrEqualTo(String value) {
            addCriterion("priority_date >=", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateLessThan(String value) {
            addCriterion("priority_date <", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateLessThanOrEqualTo(String value) {
            addCriterion("priority_date <=", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateLike(String value) {
            addCriterion("priority_date like", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateNotLike(String value) {
            addCriterion("priority_date not like", value, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateIn(List<String> values) {
            addCriterion("priority_date in", values, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateNotIn(List<String> values) {
            addCriterion("priority_date not in", values, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateBetween(String value1, String value2) {
            addCriterion("priority_date between", value1, value2, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andPriorityDateNotBetween(String value1, String value2) {
            addCriterion("priority_date not between", value1, value2, "priorityDate");
            return (Criteria) this;
        }

        public Criteria andAgentsIsNull() {
            addCriterion("agents is null");
            return (Criteria) this;
        }

        public Criteria andAgentsIsNotNull() {
            addCriterion("agents is not null");
            return (Criteria) this;
        }

        public Criteria andAgentsEqualTo(String value) {
            addCriterion("agents =", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsNotEqualTo(String value) {
            addCriterion("agents <>", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsGreaterThan(String value) {
            addCriterion("agents >", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsGreaterThanOrEqualTo(String value) {
            addCriterion("agents >=", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsLessThan(String value) {
            addCriterion("agents <", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsLessThanOrEqualTo(String value) {
            addCriterion("agents <=", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsLike(String value) {
            addCriterion("agents like", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsNotLike(String value) {
            addCriterion("agents not like", value, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsIn(List<String> values) {
            addCriterion("agents in", values, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsNotIn(List<String> values) {
            addCriterion("agents not in", values, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsBetween(String value1, String value2) {
            addCriterion("agents between", value1, value2, "agents");
            return (Criteria) this;
        }

        public Criteria andAgentsNotBetween(String value1, String value2) {
            addCriterion("agents not between", value1, value2, "agents");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoIsNull() {
            addCriterion("abstract_info is null");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoIsNotNull() {
            addCriterion("abstract_info is not null");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoEqualTo(String value) {
            addCriterion("abstract_info =", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoNotEqualTo(String value) {
            addCriterion("abstract_info <>", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoGreaterThan(String value) {
            addCriterion("abstract_info >", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoGreaterThanOrEqualTo(String value) {
            addCriterion("abstract_info >=", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoLessThan(String value) {
            addCriterion("abstract_info <", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoLessThanOrEqualTo(String value) {
            addCriterion("abstract_info <=", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoLike(String value) {
            addCriterion("abstract_info like", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoNotLike(String value) {
            addCriterion("abstract_info not like", value, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoIn(List<String> values) {
            addCriterion("abstract_info in", values, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoNotIn(List<String> values) {
            addCriterion("abstract_info not in", values, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoBetween(String value1, String value2) {
            addCriterion("abstract_info between", value1, value2, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andAbstractInfoNotBetween(String value1, String value2) {
            addCriterion("abstract_info not between", value1, value2, "abstractInfo");
            return (Criteria) this;
        }

        public Criteria andPdfAddressIsNull() {
            addCriterion("pdf_address is null");
            return (Criteria) this;
        }

        public Criteria andPdfAddressIsNotNull() {
            addCriterion("pdf_address is not null");
            return (Criteria) this;
        }

        public Criteria andPdfAddressEqualTo(String value) {
            addCriterion("pdf_address =", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressNotEqualTo(String value) {
            addCriterion("pdf_address <>", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressGreaterThan(String value) {
            addCriterion("pdf_address >", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressGreaterThanOrEqualTo(String value) {
            addCriterion("pdf_address >=", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressLessThan(String value) {
            addCriterion("pdf_address <", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressLessThanOrEqualTo(String value) {
            addCriterion("pdf_address <=", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressLike(String value) {
            addCriterion("pdf_address like", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressNotLike(String value) {
            addCriterion("pdf_address not like", value, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressIn(List<String> values) {
            addCriterion("pdf_address in", values, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressNotIn(List<String> values) {
            addCriterion("pdf_address not in", values, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressBetween(String value1, String value2) {
            addCriterion("pdf_address between", value1, value2, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andPdfAddressNotBetween(String value1, String value2) {
            addCriterion("pdf_address not between", value1, value2, "pdfAddress");
            return (Criteria) this;
        }

        public Criteria andLawStatusIsNull() {
            addCriterion("law_status is null");
            return (Criteria) this;
        }

        public Criteria andLawStatusIsNotNull() {
            addCriterion("law_status is not null");
            return (Criteria) this;
        }

        public Criteria andLawStatusEqualTo(Byte value) {
            addCriterion("law_status =", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusNotEqualTo(Byte value) {
            addCriterion("law_status <>", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusGreaterThan(Byte value) {
            addCriterion("law_status >", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("law_status >=", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusLessThan(Byte value) {
            addCriterion("law_status <", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusLessThanOrEqualTo(Byte value) {
            addCriterion("law_status <=", value, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusIn(List<Byte> values) {
            addCriterion("law_status in", values, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusNotIn(List<Byte> values) {
            addCriterion("law_status not in", values, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusBetween(Byte value1, Byte value2) {
            addCriterion("law_status between", value1, value2, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLawStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("law_status not between", value1, value2, "lawStatus");
            return (Criteria) this;
        }

        public Criteria andLogoIsNull() {
            addCriterion("logo is null");
            return (Criteria) this;
        }

        public Criteria andLogoIsNotNull() {
            addCriterion("logo is not null");
            return (Criteria) this;
        }

        public Criteria andLogoEqualTo(String value) {
            addCriterion("logo =", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotEqualTo(String value) {
            addCriterion("logo <>", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThan(String value) {
            addCriterion("logo >", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoGreaterThanOrEqualTo(String value) {
            addCriterion("logo >=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThan(String value) {
            addCriterion("logo <", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLessThanOrEqualTo(String value) {
            addCriterion("logo <=", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoLike(String value) {
            addCriterion("logo like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotLike(String value) {
            addCriterion("logo not like", value, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoIn(List<String> values) {
            addCriterion("logo in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotIn(List<String> values) {
            addCriterion("logo not in", values, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoBetween(String value1, String value2) {
            addCriterion("logo between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andLogoNotBetween(String value1, String value2) {
            addCriterion("logo not between", value1, value2, "logo");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNull() {
            addCriterion("gmt_create is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIsNotNull() {
            addCriterion("gmt_create is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreateEqualTo(Date value) {
            addCriterion("gmt_create =", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotEqualTo(Date value) {
            addCriterion("gmt_create <>", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThan(Date value) {
            addCriterion("gmt_create >", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_create >=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThan(Date value) {
            addCriterion("gmt_create <", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateLessThanOrEqualTo(Date value) {
            addCriterion("gmt_create <=", value, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateIn(List<Date> values) {
            addCriterion("gmt_create in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotIn(List<Date> values) {
            addCriterion("gmt_create not in", values, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateBetween(Date value1, Date value2) {
            addCriterion("gmt_create between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtCreateNotBetween(Date value1, Date value2) {
            addCriterion("gmt_create not between", value1, value2, "gmtCreate");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Date value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Date value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Date value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Date value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Date value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Date> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Date> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Date value1, Date value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Date value1, Date value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
package com.jahentao.patentQuery.web.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 * @author jahentao
 * @date 2018/5/30
 * @since 1.0
 */
public class StringToPathResourceConverter implements Converter<String, Resource> {
    @Override
    public Resource convert(String source) {
        return new FileSystemResource(source);
    }
}

package com.jahentao.patentQuery.web.converter;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 处理异常的解析器
 */
public class HandlerExceptionResolverImp implements HandlerExceptionResolver {

	private final Logger logger = Logger.getLogger(HandlerExceptionResolverImp.class);

	@Override
	public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response, Object handler, Exception ex) {

		// Map<String, Object> model = new HashMap<String, Object>();
		// model.put("ex", ex);
		ex.printStackTrace();
		logger.error(ex.getMessage());
		// 根据不同错误转向不同页面
		if (ex instanceof AuthorizationException) {
			return new ModelAndView("unauthorized");
		} else {
			return new ModelAndView("exception");
		}
	}

}

package com.jahentao.patentQuery.web.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 字符串到日期的转换器
 * @author jahentao
 * @date 2018/5/2
 * @since 1.0
 */
public class StringToDateConverter implements Converter<String, Date> {
    @Override
    public Date convert(String source) {
        if(StringUtils.isBlank(source)) {
            return null;
        }

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = format.parse(source.trim());
        } catch (Exception e) {
            try {
                format = new SimpleDateFormat("yyyyMMddHHmmss");
                date = format.parse(source.trim());
            } catch (Exception e1) {
                try {
                    format = new SimpleDateFormat("yyyy-MM-dd");
                    date = format.parse(source.trim());
                } catch (Exception e2) {
                }
            }
        }

        return date;
    }
}

package com.jahentao.patentQuery.web.listener;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;
import org.apache.shiro.subject.Subject;

/**
 * @author jahentao
 * @date 2018/6/24
 * @since 1.0
 */
public class MySessionListener extends SessionListenerAdapter {
    @Override
    public void onExpiration(Session session) {
        // 会话过期触发
        System.out.println("会话 "+session.getId()+" : 已过期");
    }
}

package com.jahentao.patentQuery.web.controller;


import org.slf4j.LoggerFactory;

/**
 * Created by jahentao on 2018/2/21.<br>
 * Version: 1.0
 */
public class BaseController {
    protected final org.slf4j.Logger logger =LoggerFactory.getLogger(getClass());
}

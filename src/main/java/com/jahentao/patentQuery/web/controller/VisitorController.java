package com.jahentao.patentQuery.web.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jahentao.patentQuery.constant.SysUri;
import com.jahentao.patentQuery.model.User;
import com.jahentao.patentQuery.model.bean.ResultBean;
import com.jahentao.patentQuery.model.page.QueryResult;
import com.jahentao.patentQuery.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 游客管理控制器
 * @author jahentao
 * @date 2018/6/25
 * @since 1.0
 */
@Controller
@RequestMapping(SysUri.VISITOR)
public class VisitorController extends BaseController{

    @Autowired
    private UserService userService;

    // =============================页面跳转===================================

    @RequestMapping(value = "list")
    public String list() {
        return "admin/visitor/list";
    }

    // =============================流程控制===================================

    @ResponseBody
    @RequestMapping(value = "getList",method = RequestMethod.POST)
    public ResultBean getList(@RequestParam(value = "pageNumber", defaultValue = "1")Integer pageNumber,
                              @RequestParam(value = "pageSize", defaultValue = "10")Integer pageSize) {
        ResultBean rb = new ResultBean();

        // 分页查询
        PageHelper.startPage(pageNumber, pageSize);
        final List<User> patentDemands = userService.getVisitor();

        Page patentDemandsPage = (Page) patentDemands;
        QueryResult<User> queryResult = new QueryResult<User>();
        queryResult.setCurrentPage(pageNumber);
        queryResult.setShowNum(pageSize);
        queryResult.setPages(patentDemandsPage.getTotal(), pageSize);
        queryResult.setItems(patentDemands);

        // 返回结果
        rb.setData(queryResult);

        return rb;
    }
}

package com.jahentao.patentQuery.web.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jahentao.patentQuery.constant.SysUri;
import com.jahentao.patentQuery.model.Admin;
import com.jahentao.patentQuery.model.AdminExample;
import com.jahentao.patentQuery.model.bean.ResultBean;
import com.jahentao.patentQuery.model.page.QueryResult;
import com.jahentao.patentQuery.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 将管理员登录整合进用户登录中，所以管理员登录被废弃了
 * @author jahentao
 * @date 2018/4/15
 * @since 1.0
 */
@Deprecated
@Controller
@RequestMapping(SysUri.ADMIN)
public class AdminController extends BaseController {

    @Autowired
    private AdminService adminService;

    // =============================页面跳转===================================

    @RequestMapping(value = "hello")
    public String hello() {
        return "admin/hello";
    }

    @RequestMapping(value = "list")
    public String list() {
        return "admin/administrator/list";
    }

    @RequestMapping(value = "edit")
    public String edit() {
        return "admin/administrator/edit";
    }

//    @RequestMapping(value = "add")
//    public String add() {
//        return "admin/add";
//    }

    // =============================流程控制===================================

    /**
     * 处理登录
     * <p>验证用户名密码是否正确</p>
     */
    @RequestMapping(value = "login",method = RequestMethod.POST)
    public String login(ModelAndView mv, Admin admin){
        // 用户登录
        Admin rs=adminService.login(admin);

        if(rs!=null){
            return "admin/admin";
        }else{
            return null;//用户名或者密码不正确！
        }
    }

    @RequestMapping(value = "getList",method = RequestMethod.POST)
    public @ResponseBody ResultBean getList(@RequestParam(value = "pageNumber", defaultValue = "1")Integer pageNumber,
                                            @RequestParam(value = "pageSize", defaultValue = "10")Integer pageSize) {
        ResultBean rb = new ResultBean();

        AdminExample adminExample = new AdminExample();

        // 分页查询
        PageHelper.startPage(pageNumber, pageSize);
        final List<Admin> admins = adminService.selectByExample(adminExample);

        Page adminsPage = (Page) admins;
        QueryResult<Admin> queryResult = new QueryResult<Admin>();
        queryResult.setCurrentPage(pageNumber);
        queryResult.setPages(adminsPage.getTotal(), pageSize);
        queryResult.setItems(admins);

        // 返回结果
        rb.setData(queryResult);

        return rb;
    }

}

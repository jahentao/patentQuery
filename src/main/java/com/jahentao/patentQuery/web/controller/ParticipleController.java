package com.jahentao.patentQuery.web.controller;

import com.jahentao.patentQuery.constant.SysUri;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jahentao
 * @date 2018/4/14
 * @since 1.0
 */
@Controller
@RequestMapping(SysUri.PARTICIPLE)
public class ParticipleController extends BaseController {
}

package com.jahentao.patentQuery.web.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jahentao.patentQuery.config.FileUploadConfig;
import com.jahentao.patentQuery.config.ParticipleConfig;
import com.jahentao.patentQuery.constant.EhCacheKeys;
import com.jahentao.patentQuery.constant.SysUri;
import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.model.*;
import com.jahentao.patentQuery.model.bean.MessageCode;
import com.jahentao.patentQuery.model.bean.ResultBean;
import com.jahentao.patentQuery.model.page.QueryResult;
import com.jahentao.patentQuery.model.progress.AbstractTask;
import com.jahentao.patentQuery.model.progress.Progress;
import com.jahentao.patentQuery.service.*;
import com.jahentao.patentQuery.util.ReadExcel;
import org.ansj.domain.Result;
import org.ansj.domain.Term;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * <p>专利提供业务相关 控制器</p>
 * <p>业务上，找求购，对于工作人员来说即找买家。【卖家提供案子，买家求购案子】后半句。</p>
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
@Controller
@RequestMapping(SysUri.DEMAND)
public class PatentDemandController extends BaseController {

    @Autowired
    private PatentService patentService;
    @Autowired
    private PatentDemandService patentDemandService;
    @Autowired
    private PatentSupplyService patentSupplyService;
    @Autowired
    private ParticipleService participleService;
    @Autowired
    private UserService userService;
    @Autowired
    private PatentDemandQueryService patentDemandQueryService;

    @Autowired
    private ParticipleConfig participleConfig;
    @Autowired
    private FileUploadConfig fileUploadConfig;

    @Resource
    private CacheManager springCacheManager;

    /**
     * 任务编号。
     * <p>有个缺点，重启应用之后，该编号又要从1开始</p>
     * <p>也罢，就限定任务在重启之后，全部失效</p>
     */
    static final AtomicLong TASK_ID_NUMBER = new AtomicLong(1);

    // =============================页面跳转===================================

    @RequestMapping(value = "list")
    public String list() {
        return "admin/patentDemand/list";
    }

    @RequestMapping(value = "edit")
    public String edit() {
        return "admin/patentDemand/edit";
    }

    @RequestMapping(value = "add")
    public String add() {
        return "admin/patentDemand/add";
    }

    // =============================流程控制===================================

    /**
     * 搜索潜在客户,并分页显示
     * @param patent 卖家提供的专利信息
     * @param start 起始时间
     * @param end 终止时间
     * @param pageSize 每页显示记录数
     * @param pageNumber 当前页号
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "search",method = RequestMethod.POST)
    public ResultBean search(Patent patent,
                             @RequestParam(value = "start") String start, @RequestParam(value = "end") String end,
                             @RequestParam(value = "pageNumber", defaultValue = "1")Integer pageNumber,
                             @RequestParam(value = "pageSize", defaultValue = "10")Integer pageSize,
                             @RequestParam(value = "selectAll", defaultValue = "false")Boolean selectAll) {
        logger.debug("工作人员替卖家找求购，专利号："+patent.getId()+", 专利名: "+patent.getName());
        logger.debug("批量转换，是否全部转换：" + selectAll);
        if (!selectAll) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            logger.debug("转换时间选择：" + formatter.format(start) + " - " + formatter.format(end));
        }
        ResultBean rb = new ResultBean();

        // TODO 获取userId，整合进shiro
        Long userId = 1L;// userid目前默认为1
        User user = new User();
        user.setId(userId);

        // 专利号和查询记录都不能为空

        if (StringUtils.isEmpty(patent.getId())) {

            if (patent.getId().length() > 14) {
                rb.setMessageCode(MessageCode.INCORRECT_PATENT_ID);
                rb.setMessage("专利号不正确");
                return rb;
            }

            rb.setMessageCode(MessageCode.PATENT_ID_NULL);
            rb.setMessage("专利号为空");
            return rb;
        }

        if (StringUtils.isEmpty(patent.getName())) {
            rb.setMessageCode(MessageCode.PATENT_NAME_NULL);
            rb.setMessage("专利名称为空");
            return rb;
        }


        // 查询记录插入数据库
        PatentDemandQuery entity = new PatentDemandQuery(user, patent.getId(), patent.getName(), new Date());
        patentDemandQueryService.save(entity);

//        LocalDate startDate = LocalDate.parse(start);
//        LocalDate endDate = LocalDate.parse(end);

        // 分词
        Result result = participleService.parse(patent.getName());

        List<Term> terms = result.getTerms(); //拿到terms
        Set<String> participleResultSet = new HashSet<>();// 用于查询的分词结果集

        for(int i=0; i<terms.size(); i++) {
            String word = terms.get(i).getName(); //拿到词
            String natureStr = terms.get(i).getNatureStr(); //拿到词性
            if(participleConfig.getExpectedNature().contains(natureStr)) {
                logger.debug("分词："+word);
                participleResultSet.add(word);
            }
        }

        // fix 输入无效数字，查询participleResultSet.size() = 0时，查出所有记录数问题
        if (participleResultSet.size() == 0) {
            rb.setMessageCode(MessageCode.SUCCESS);
            rb.setMessage("没有查询到该专利的相关信息");
            return rb;
        }

        // 循环分词结果集，对每一项进行查询
        PatentDemandExample patentDemandExample = new PatentDemandExample();

        if (!selectAll) {
            patentDemandExample.setStart(start);
            patentDemandExample.setEnd(end);
        }

        patentDemandExample.setOrderByClause("demand_time desc");
        for (String s : participleResultSet) {
            patentDemandExample.or().andContentLike("%"+s+"%");
        }

        // 分页查询
        PageHelper.startPage(pageNumber, pageSize);
        final List<PatentDemand> patentDemands = patentDemandService.selectByExample(patentDemandExample);

        Page patentDemandsPage = (Page) patentDemands;
        QueryResult<PatentDemand> queryResult = new QueryResult<PatentDemand>();
        queryResult.setCurrentPage(pageNumber);
        queryResult.setShowNum(pageSize);
        queryResult.setPages(patentDemandsPage.getTotal(), pageSize);
        queryResult.setItems(patentDemands);

        // 返回结果
        rb.setMessage(terms.toString());
        rb.setData(queryResult);

        return rb;
    }

    @ResponseBody
    @RequestMapping(value = "getList",method = RequestMethod.POST)
    public ResultBean getList(@RequestParam(value = "pageNumber", defaultValue = "1")Integer pageNumber,
                             @RequestParam(value = "pageSize", defaultValue = "10")Integer pageSize) {
        ResultBean rb = new ResultBean();

//        PatentDemandExample patentDemandExample = new PatentDemandExample();
//        patentDemandExample.setOrderByClause("demand_time desc");

        // 分页查询
        PageHelper.startPage(pageNumber, pageSize);
        final List<PatentDemand> patentDemands = patentDemandService.getAll();

        Page patentDemandsPage = (Page) patentDemands;
        QueryResult<PatentDemand> queryResult = new QueryResult<PatentDemand>();
        queryResult.setCurrentPage(pageNumber);
        queryResult.setShowNum(pageSize);
        queryResult.setPages(patentDemandsPage.getTotal(), pageSize);
        queryResult.setItems(patentDemands);

        // 返回结果
        rb.setData(queryResult);

        return rb;
    }

    @ResponseBody
    @RequestMapping(value = "addDemand", method = RequestMethod.POST)
    public ResultBean addDemand(@RequestParam("content") String content, @RequestParam("qq") String qq,
                                @RequestParam("demandTime") String demandTime) {
        ResultBean rb = new ResultBean();

        PatentDemand demand = new PatentDemand();
        demand.setContent(content);
        // LocalDate 转 Date
        LocalDate localDate = LocalDate.parse(demandTime);
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDate.atStartOfDay(zoneId);
        Date date = Date.from(zdt.toInstant());
        demand.setDemandTime(date);

        // 根据QQ号查询用户
        UserExample userExample = new UserExample();
        userExample.createCriteria().andQqEqualTo(qq);
        List<User> users = userService.selectByExample(userExample);
        if (users==null || users.size()==0) { // 数据库中无用户QQ记录
            User user = new User();
            user.setQq(qq);
            userService.save(user);
            demand.setUser(user);
        }else {
            User user = users.get(0);
            demand.setUser(user);
        }

        // 新增需求记录
        final int saved = patentDemandService.save(demand);

        if (saved<=0) {
            rb.setMessage("保存失败");
        }else {
            rb.setMessage("保存成功");
        }
        return rb;
    }

    @ResponseBody
    @RequestMapping(value = "import", method = RequestMethod.POST)
    public ResultBean batchImport(@RequestParam("excelFile") MultipartFile excelFile) {
        ResultBean rb = new ResultBean();

        Map<String, Object> data = new HashMap<String, Object>();
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_BATCH_IMPORT_TASK_CACHE);

        long taskId = TASK_ID_NUMBER.getAndIncrement();
        data.put("taskId", taskId);

        try {
            patentDemandService.asyncTask(new AbstractTask(taskId) {
                @Override
                public void execute() {
                    logger.debug("task {} is executing.", taskId);
                    InputStream inputs;
                    FileOutputStream out = null;
                    ReadExcel readExcel = null;
                    StringBuffer stringBuffer = null;
                    int successRows = 0;
                    int errorRows = 0;

                    try {
                        Date today = new Date();
                        //上传
                        inputs = excelFile.getInputStream();
                        String fileName = excelFile.getOriginalFilename();
                        File file = new File(fileUploadConfig.getUploadPath()+ today.getTime() + " - " + fileName);
                        FileUtils.copyInputStreamToFile(inputs, file);
                        logger.debug("文件 {} 上传成功", excelFile.getOriginalFilename());

                        // 解析Excel，导入MySQL
                        readExcel=new ReadExcel(2);
                        // 记录错误
                        File errorRecordFile = new File(fileUploadConfig.getUploadPath() + "错误记录-" + today.getTime() + ".txt");
                        out = new FileOutputStream(errorRecordFile);
                        OutputStreamWriter outWriter = new OutputStreamWriter(out, "UTF-8");
                        BufferedWriter bufWrite = new BufferedWriter(outWriter);

                        // 读取Excel文件
                        readExcel.getExcelWorkbook(file.getAbsolutePath());

                        // 进度
                        Progress progress = new Progress();
                        progress.setTotal(readExcel.getTotalRows() - 1);
                        progress.setCompleted(0);
                        progress.setPercentage(0F);

                        UserExample userExample = new UserExample();

                        for(int i = 1; i < readExcel.getTotalRows(); i++) {
                            List<String> list = readExcel.getLine(i);

                            if (list == null || list.size() < 2) {
                                break;
                            }

                            PatentDemand patentDemand = new PatentDemand();
                            patentDemand.setDemandTime(today);
                            patentDemand.setContent(list.get(0));

                            // 根据QQ查找用户
                            userExample.clear();
                            userExample.createCriteria().andQqEqualTo(list.get(1)); // QQ 号
                            List<User> users = userService.selectByExample(userExample);
                            if (users==null || users.size()==0) { // 数据库中无用户QQ记录
                                User user = new User();
                                user.setQq(list.get(1));
                                userService.save(user);
                                patentDemand.setUser(user);
                            }else {
                                User user = users.get(0);
                                patentDemand.setUser(user);
                            }

                            final int saved = patentDemandService.save(patentDemand);
                            if (saved > 0) {
                                successRows ++ ;
                            } else {
                                errorRows ++;
                            }

                            // 每处理完patent_demand_query中1条记录，在cache中更新记录
                            progress.setCompleted(progress.getCompleted() + 1);
                            // 根据taskId，设置进度
                            logger.debug("task {} 目前进度：{}", taskId, progress.getPercentage());
                            cache.put(taskId, progress.getPercentage());
                        }
                        bufWrite.write("------------------");
                        bufWrite.write("成功导入"+successRows+"条");
                        bufWrite.flush();

                        stringBuffer = new StringBuffer("成功导入" + successRows + "条");
                        if (successRows + 1 != readExcel.getTotalRows()) {
                            stringBuffer.append("，详见错误记录");
                        }

                        cache.put(getId() + "-message", stringBuffer.toString());
                        cache.put(getId() + "-errorRecordFileName", errorRecordFile.getName());

                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (readExcel != null) {
                            readExcel.closeStream();
                        }
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        } catch (TaskException e) {
            e.printStackTrace();
        }

        rb.setData(data);
        return rb;
    }

    @ResponseBody
    @RequestMapping(value = "getProgress", method = RequestMethod.POST)
    public ResultBean getProgress(Long taskId) {
        ResultBean rb = new ResultBean();
        Map<String, Object> data = new HashMap<String, Object>(2);
        String process = patentDemandService.getProcess(taskId);
        // 将清理缓存的任务交给查看进度
        if (process.startsWith(Progress.COMPLETE_PERCENTAGE)) {
            patentDemandService.clearCache(taskId);

            Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_BATCH_IMPORT_TASK_CACHE);
            data.put("message", cache.get(taskId + "-message").get());
            data.put("fileName", cache.get(taskId + "-errorRecordFileName").get());
        }
        data.put("taskId", taskId);
        data.put("progress", process);
        rb.setData(data);
        return rb;
    }

    @RequestMapping("download")
    public void downFile(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        // 得到要下载的文件名
        try {
//            fileName = new String(fileName.getBytes("UTF-8"), "UTF-8");
            // 得到要下载的文件
            File file = new File(fileUploadConfig.getUploadPath() + fileName);

            // 如果文件不存在
            if (!file.exists()) {
                // 文件已被删除！！
                return;
            }
            // 处理文件名
//            String realname = fileName.substring(0, fileName.lastIndexOf("."));
            // 设置响应头，控制浏览器下载该文件
            response.setHeader("content-disposition", "attachment;filename="
                    + URLEncoder.encode(fileName, "UTF-8"));
            // 读取要下载的文件，保存到文件输入流
            FileInputStream in = new FileInputStream(file);
            // 创建输出流
            OutputStream out = response.getOutputStream();
            // 创建缓冲区
            byte buffer[] = new byte[1024];
            int len = 0;
            // 循环将输入流中的内容读取到缓冲区当中
            while ((len = in.read(buffer)) > 0) {
                // 输出缓冲区的内容到浏览器，实现文件下载
                out.write(buffer, 0, len);
            }
            // 关闭文件输入流
            in.close();
            // 关闭输出流
            out.close();
        } catch (Exception e) {

        }
    }

    @ResponseBody
    @RequestMapping(value = "deleteIds", method = RequestMethod.POST)
    public ResultBean deleteIds(@RequestParam(value = "ids[]") Long[] ids) {
        ResultBean rb = new ResultBean();

        boolean allSuccess = true;

        for (Long id : ids) {
            final int deleted = patentDemandService.delete(id);
            allSuccess &= deleted > 0;
        }

        if (allSuccess) {
            rb.setMessageCode(MessageCode.SUCCESS);
            rb.setMessage("都删除成功");
        }else {
            rb.setMessageCode(MessageCode.DELETE_FAILURE);
            rb.setMessage("删除失败，或部分删除成功");
        }

        return rb;
    }

}

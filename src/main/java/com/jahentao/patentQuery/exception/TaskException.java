package com.jahentao.patentQuery.exception;

/**
 * @author jahentao
 * @date 2018/6/1
 * @since 1.0
 */
public class TaskException extends Exception {
    public TaskException(String message) {
        super(message);
    }
}

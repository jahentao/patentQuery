package com.jahentao.patentQuery.config;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author jahentao
 * @date 2018/4/14
 * @since 1.0
 */
public class ParticipleConfig {
    private Set<String> expectedNature;

    public Set<String> getExpectedNature() {
        return expectedNature;
    }

    public void setExpectedNature(Set<String> expectedNature) {
        this.expectedNature = expectedNature;
    }
}

package com.jahentao.patentQuery.config;

/**
 * @author jahentao
 * @date 2018/5/2
 * @since 1.0
 */
public class FileUploadConfig {
    /**
     * 上传文件保存路径
     */
    private String uploadPath;

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
}

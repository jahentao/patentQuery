package com.jahentao.patentQuery.constant;

/**
 * @author jahentao
 * @date 2018/5/25
 * @since 1.0
 */
public class Constants {
    
    public static final String CURRENT_USER = "user";
    /**
     * 连续登录失败次数后，需要输入验证码
     */
    public static final Integer USER_LOGIN_COUNT_NEED_CAPTCHA = 3;
    /**
     * 用户登录失败，被锁定的最大次数
     */
    public static final Integer USER_LOGIN_MAX_COUNT_LOCK=10;
}

package com.jahentao.patentQuery.constant;

/**
 * @author jahentao
 * @date 2018/4/14
 * @since 1.0
 */
public class SysUri {
    public static final String DEMAND = "demand";
    public static final String SUPPLY = "supply";
    public static final String DEMAND_QUERY = "demandQuery";
    public static final String SUPPLY_QUERY = "supplyQuery";
    public static final String PARTICIPLE = "participle";
    public static final String SALE = "sale";
    public static final String USER = "user";
    public static final String VISITOR = "visitor";
    public static final String ADMIN = "admin";
}

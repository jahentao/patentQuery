package com.jahentao.patentQuery.constant;

/**
 * @author jahentao
 */
public class EhCacheKeys {

	/**
	 * 登录记录缓存
	 */
	public static final String LOGIN_LOG_CACHE ="loginLogCache";
	/**
	 * 长时间的异步任务，将patent_demand_query导入patent_supply转换的cache
	 */
	public static final String ASYNC_DEMAND_QUERY_IMPORT_TO_SUPPLY_TASK_CACHE = "asyncDemandQueryImportToSupplyTaskCache";
    /**
     * 长时间的异步任务，将patent_demand_query导入patent_supply转换的cache
     */
    public static final String ASYNC_SUPPLY_QUERY_IMPORT_TO_DEMAND_TASK_CACHE = "asyncSupplyQueryImportToDemandTaskCache";
	/**
	 * 长时间的异步任务，批量导入需求信息的cache
	 */
	public static final String ASYNC_DEMAND_BATCH_IMPORT_TASK_CACHE = "asyncDemandBatchImportTaskCache";
	/**
	 * 长时间的异步任务，批量导入专利信息的cache
	 */
    public static final String ASYNC_SUPPLY_BATCH_IMPORT_TASK_CACHE = "asyncSupplyBatchImportTaskCache";
	/**
	 * 手机短信或邮件的验证码缓存
	 */
	public static final String VERIFICATION_CODE_CACHE = "verificationCodeCache";

	/**
	 * 菜单cache
	 */
	public static final String SYS_MEMU="sys_memu";
	/**
	 * 用户cache
	 */
	public static final String SYS_USER_BEAN="sys_user_bean";

}

package com.jahentao.patentQuery.constant;

import java.util.Random;

/**
 * 验证码
 * @author jahentao
 * @date 2018/6/19
 * @since 1.0
 */
public class VerificationCode {

    // 验证码类型
    /** 短信验证码*/
    public static final String SMS_TYPE = "sms";
    /** 邮件验证码*/
    public static final String MAIL_TYPE = "mail";

    /**
     * 生成6位随机验证码
     * <p>用于短信验证码和邮件验证码的生成</p>
     * @return 验证码
     */
    public static String generateCode() {
        return String.valueOf(new Random().nextInt(899999) + 100000);
    }
}

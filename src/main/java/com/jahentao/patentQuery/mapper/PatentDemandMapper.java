package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.PatentDemand;
import com.jahentao.patentQuery.model.PatentDemandExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentDemandMapper {
    long countByExample(PatentDemandExample example);

    int deleteByExample(PatentDemandExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatentDemand record);

    int insertSelective(PatentDemand record);

    List<PatentDemand> selectByExample(PatentDemandExample example);

    PatentDemand selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatentDemand record, @Param("example") PatentDemandExample example);

    int updateByExample(@Param("record") PatentDemand record, @Param("example") PatentDemandExample example);

    int updateByPrimaryKeySelective(PatentDemand record);

    int updateByPrimaryKey(PatentDemand record);

    // =========================新增===============================================

    List<PatentDemand> getAll();
}
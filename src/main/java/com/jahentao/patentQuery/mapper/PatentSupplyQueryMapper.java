package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.PatentSupplyQuery;
import com.jahentao.patentQuery.model.PatentSupplyQueryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentSupplyQueryMapper {
    long countByExample(PatentSupplyQueryExample example);

    int deleteByExample(PatentSupplyQueryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatentSupplyQuery record);

    int insertSelective(PatentSupplyQuery record);

    List<PatentSupplyQuery> selectByExample(PatentSupplyQueryExample example);

    PatentSupplyQuery selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatentSupplyQuery record, @Param("example") PatentSupplyQueryExample example);

    int updateByExample(@Param("record") PatentSupplyQuery record, @Param("example") PatentSupplyQueryExample example);

    int updateByPrimaryKeySelective(PatentSupplyQuery record);

    int updateByPrimaryKey(PatentSupplyQuery record);
}
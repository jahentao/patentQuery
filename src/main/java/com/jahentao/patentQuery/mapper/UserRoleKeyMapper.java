package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.UserRoleKey;
import com.jahentao.patentQuery.model.UserRoleKeyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserRoleKeyMapper {
    long countByExample(UserRoleKeyExample example);

    int deleteByExample(UserRoleKeyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserRoleKey record);

    int insertSelective(UserRoleKey record);

    List<UserRoleKey> selectByExample(UserRoleKeyExample example);

    UserRoleKey selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserRoleKey record, @Param("example") UserRoleKeyExample example);

    int updateByExample(@Param("record") UserRoleKey record, @Param("example") UserRoleKeyExample example);

    int updateByPrimaryKeySelective(UserRoleKey record);

    int updateByPrimaryKey(UserRoleKey record);
}
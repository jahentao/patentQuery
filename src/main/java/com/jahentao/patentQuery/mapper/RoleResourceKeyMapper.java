package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.RoleResourceKey;
import com.jahentao.patentQuery.model.RoleResourceKeyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleResourceKeyMapper {
    long countByExample(RoleResourceKeyExample example);

    int deleteByExample(RoleResourceKeyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RoleResourceKey record);

    int insertSelective(RoleResourceKey record);

    List<RoleResourceKey> selectByExample(RoleResourceKeyExample example);

    RoleResourceKey selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RoleResourceKey record, @Param("example") RoleResourceKeyExample example);

    int updateByExample(@Param("record") RoleResourceKey record, @Param("example") RoleResourceKeyExample example);

    int updateByPrimaryKeySelective(RoleResourceKey record);

    int updateByPrimaryKey(RoleResourceKey record);
}
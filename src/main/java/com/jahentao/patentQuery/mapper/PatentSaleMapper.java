package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.PatentSale;
import com.jahentao.patentQuery.model.PatentSaleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentSaleMapper {
    long countByExample(PatentSaleExample example);

    int deleteByExample(PatentSaleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatentSale record);

    int insertSelective(PatentSale record);

    List<PatentSale> selectByExample(PatentSaleExample example);

    PatentSale selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatentSale record, @Param("example") PatentSaleExample example);

    int updateByExample(@Param("record") PatentSale record, @Param("example") PatentSaleExample example);

    int updateByPrimaryKeySelective(PatentSale record);

    int updateByPrimaryKey(PatentSale record);
}
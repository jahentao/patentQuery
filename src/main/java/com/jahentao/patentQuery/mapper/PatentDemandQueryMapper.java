package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.PatentDemandQuery;
import com.jahentao.patentQuery.model.PatentDemandQueryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentDemandQueryMapper {
    long countByExample(PatentDemandQueryExample example);

    int deleteByExample(PatentDemandQueryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatentDemandQuery record);

    int insertSelective(PatentDemandQuery record);

    List<PatentDemandQuery> selectByExample(PatentDemandQueryExample example);

    PatentDemandQuery selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatentDemandQuery record, @Param("example") PatentDemandQueryExample example);

    int updateByExample(@Param("record") PatentDemandQuery record, @Param("example") PatentDemandQueryExample example);

    int updateByPrimaryKeySelective(PatentDemandQuery record);

    int updateByPrimaryKey(PatentDemandQuery record);
}
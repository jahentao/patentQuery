package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.PatentExample;
import com.jahentao.patentQuery.model.PatentSupply;
import com.jahentao.patentQuery.model.PatentSupplyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentSupplyMapper {
    long countByExample(PatentSupplyExample example);

    int deleteByExample(PatentSupplyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PatentSupply record);

    int insertSelective(PatentSupply record);

    List<PatentSupply> selectByExample(PatentSupplyExample example);

    PatentSupply selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PatentSupply record, @Param("example") PatentSupplyExample example);

    int updateByExample(@Param("record") PatentSupply record, @Param("example") PatentSupplyExample example);

    int updateByPrimaryKeySelective(PatentSupply record);

    int updateByPrimaryKey(PatentSupply record);

    // ========================Patent-PatentSupply================================

    List<PatentSupply> selectByPatentExample(PatentExample example);

    // =========================新增===============================================

    List<PatentSupply> getAll();

}
package com.jahentao.patentQuery.mapper;

import com.jahentao.patentQuery.model.Patent;
import com.jahentao.patentQuery.model.PatentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PatentMapper {
    long countByExample(PatentExample example);

    int deleteByExample(PatentExample example);

    int deleteByPrimaryKey(String id);

    int insert(Patent record);

    int insertSelective(Patent record);

    List<Patent> selectByExample(PatentExample example);

    Patent selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Patent record, @Param("example") PatentExample example);

    int updateByExample(@Param("record") Patent record, @Param("example") PatentExample example);

    int updateByPrimaryKeySelective(Patent record);

    int updateByPrimaryKey(Patent record);
}
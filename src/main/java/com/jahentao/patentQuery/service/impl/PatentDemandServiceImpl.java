package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.constant.EhCacheKeys;
import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.mapper.PatentDemandMapper;
import com.jahentao.patentQuery.model.PatentDemand;
import com.jahentao.patentQuery.model.PatentDemandExample;
import com.jahentao.patentQuery.model.progress.AbstractTask;
import com.jahentao.patentQuery.service.PatentDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
@Service
public class PatentDemandServiceImpl extends BaseServiceImpl  implements PatentDemandService {

    @Autowired
    private PatentDemandMapper patentDemandMapper;
    @Resource
    private CacheManager springCacheManager;

    @Override
    public PatentDemand selectByKey(Long key) {
        return patentDemandMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(PatentDemand entity) {
        return patentDemandMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentDemandMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(PatentDemand entity) {
        return patentDemandMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(PatentDemand entity) {
        return patentDemandMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<PatentDemand> selectByExample(Object example) {
        return patentDemandMapper.selectByExample((PatentDemandExample) example);
    }

    @Override
    public List<PatentDemand> getAll() {
        return patentDemandMapper.getAll();
    }

    // ========================重构为异步任务================================

    @Async
    @Override
    public void asyncTask(AbstractTask task) throws TaskException {
        task.execute();
    }

    @Override
    public String getProcess(Long taskId) {
        if (taskId == null) {
            return "0";
        }
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_BATCH_IMPORT_TASK_CACHE);
        if (cache.get(taskId) == null) {
            return "0";
        }
        String progress = cache.get(taskId).get().toString();
        logger.debug("获取task {} 进度：{}", taskId, progress);
        return progress;
    }

    @Override
    public void clearCache(Long taskId) {
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_BATCH_IMPORT_TASK_CACHE);
        logger.debug("task {} 进度完成，清除缓存", taskId);
        cache.evict(taskId);
    }
}

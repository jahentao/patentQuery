package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.PatentDemandMapper;
import com.jahentao.patentQuery.mapper.PatentMapper;
import com.jahentao.patentQuery.mapper.PatentSupplyMapper;
import com.jahentao.patentQuery.model.Patent;
import com.jahentao.patentQuery.model.PatentDemand;
import com.jahentao.patentQuery.model.PatentExample;
import com.jahentao.patentQuery.model.PatentSupply;
import com.jahentao.patentQuery.service.PatentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.soap.Addressing;
import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
@Service
public class PatentServiceImpl extends BaseServiceImpl  implements PatentService {

    @Autowired
    private PatentMapper patentMapper;

    @Override
    public Patent selectByKey(Long key) {
        return patentMapper.selectByPrimaryKey(String.valueOf(key));
    }

    @Override
    public Integer save(Patent entity) {
        return patentMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentMapper.deleteByPrimaryKey(String.valueOf(key));
    }

    @Override
    public Integer updateAll(Patent entity) {
        return patentMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(Patent entity) {
        return patentMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<Patent> selectByExample(Object example) {
        return patentMapper.selectByExample((PatentExample) example);
    }
}

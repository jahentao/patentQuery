package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.constant.EhCacheKeys;
import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.mapper.PatentDemandQueryMapper;
import com.jahentao.patentQuery.model.PatentDemandQuery;
import com.jahentao.patentQuery.model.PatentDemandQueryExample;
import com.jahentao.patentQuery.model.progress.AbstractTask;
import com.jahentao.patentQuery.service.PatentDemandQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/30
 * @since 1.0
 */
@Service
public class PatentDemandQueryServiceImpl extends BaseServiceImpl implements PatentDemandQueryService {

    @Autowired
    private PatentDemandQueryMapper patentDemandQueryMapper;
    @Resource
    private CacheManager springCacheManager;

    @Override
    public PatentDemandQuery selectByKey(Long key) {
        return patentDemandQueryMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(PatentDemandQuery entity) {
        return patentDemandQueryMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentDemandQueryMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(PatentDemandQuery entity) {
        return patentDemandQueryMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(PatentDemandQuery entity) {
        return patentDemandQueryMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<PatentDemandQuery> selectByExample(Object example) {
        return patentDemandQueryMapper.selectByExample((PatentDemandQueryExample) example);
    }

    ///////////////// 执行转换任务时，提供的服务
    // 现在还不具备抽象的基础。抽象的基础是什么？
    // 我不能一上来就急着抽象。我得先写业务，从重构的角度来抽象

    // Fix 异步方法，会产生一个问题，taskId为空
    // 所以task Id 得通过Task外部的方法产生
    @Async
    @Override
    public void asyncTask(AbstractTask task) throws TaskException {
        task.execute();
    }

    @Override
    public String getProcess(Long taskId){
        if (taskId == null) {
            return "0";
        }
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_QUERY_IMPORT_TO_SUPPLY_TASK_CACHE);
        // 对Cache中元素的访问，涉及并发吗？
        // 反正页面ajax轮询，获取进度，这次没获取到，下次能获取到
        if (cache.get(taskId) == null) {
            return "0";
        }
        String progress = cache.get(taskId).get().toString();
        logger.debug("获取task {} 进度：{}", taskId, progress);
        return progress;
    }

    @Override
    public void clearCache(Long taskId) {
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_DEMAND_QUERY_IMPORT_TO_SUPPLY_TASK_CACHE);
        logger.debug("task {} 进度完成，清除缓存", taskId);
        cache.evict(taskId);
    }
}

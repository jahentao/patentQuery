package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.service.ParticipleService;
import org.ansj.domain.Result;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author jahentao
 * @date 2018/4/14
 * @since 1.0
 */
@Service
public class ParticipleServiceImpl extends BaseServiceImpl implements ParticipleService {
    @Override
    public Result parse(String str) {
        //只关注这些词性的词
//        Set<String> expectedNature = new HashSet<String>() {{ //1.8语法
//            add("n");add("v");add("vd");add("vn");add("vf");
//            add("vx");add("vi");add("vl");add("vg");
//            add("nt");add("nz");add("nw");add("nl");
//            add("ng");add("userDefine");add("wh");
//        }};
        // 使用Ansj分词
        return ToAnalysis.parse(str); //精准分词结果的一个封装，主要是一个List<Term>的terms
    }
}

package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.model.User;
import com.jahentao.patentQuery.service.UserLoginService;
import com.jahentao.patentQuery.web.shiro.TokenBuild;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

/**
 * @author jahentao
 * @date 2018/5/26
 * @since 1.0
 */
@Service
public class UserLoginServiceImpl extends BaseServiceImpl  implements UserLoginService {

    @Override
    public void loginWithUsername(User user, Boolean rememberMe, String host) {
        // 构造登陆令牌环
        TokenBuild token = new TokenBuild(user.getUsername(),
                user.getPassword().toCharArray(), rememberMe, host);

        // 发出登陆请求
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);
    }

    @Override
    public void logout(User user) {

    }
}

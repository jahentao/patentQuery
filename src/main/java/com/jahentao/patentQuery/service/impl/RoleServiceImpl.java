package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.RoleMapper;
import com.jahentao.patentQuery.model.Role;
import com.jahentao.patentQuery.model.RoleExample;
import com.jahentao.patentQuery.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/5/26
 * @since 1.0
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl  implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Role selectByKey(Long key) {
        return roleMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(Role entity) {
        return roleMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return roleMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(Role entity) {
        return roleMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(Role entity) {
        return roleMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<Role> selectByExample(Object example) {
        return roleMapper.selectByExample((RoleExample) example);
    }
}

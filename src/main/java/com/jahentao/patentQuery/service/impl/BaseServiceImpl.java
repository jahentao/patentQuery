package com.jahentao.patentQuery.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author jahentao
 * @date 2018/5/31
 * @since 1.0
 */
public class BaseServiceImpl {
    // 根据阿里巴巴Java编码规范 二、（二）日志规约，1. 【强制】
    // 应用中不可直接使用日志系统（Log4j、Logback）中的API，而应依赖使用日志框架
    // SLF4J 中的API，使用门面模式的日志框架，有利于维护和各个类的日志处理方式统一。
    /** 日志*/
    protected final Logger logger =LoggerFactory.getLogger(getClass());
}

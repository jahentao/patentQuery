package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.constant.EhCacheKeys;
import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.mapper.PatentSupplyMapper;
import com.jahentao.patentQuery.model.PatentExample;
import com.jahentao.patentQuery.model.PatentSupply;
import com.jahentao.patentQuery.model.PatentSupplyExample;
import com.jahentao.patentQuery.model.progress.AbstractTask;
import com.jahentao.patentQuery.service.PatentSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
@Service
public class PatentSupplyServiceImpl extends BaseServiceImpl  implements PatentSupplyService {

    @Autowired
    private PatentSupplyMapper patentSupplyMapper;
    @Resource
    private CacheManager springCacheManager;

    @Override
    public PatentSupply selectByKey(Long key) {
        return patentSupplyMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(PatentSupply entity) {
        return patentSupplyMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentSupplyMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(PatentSupply entity) {
        return patentSupplyMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(PatentSupply entity) {
        return patentSupplyMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<PatentSupply> selectByExample(Object example) {
        return patentSupplyMapper.selectByExample((PatentSupplyExample) example);
    }

    // ========================Patent-PatentSupply================================

    @Override
    public List<PatentSupply> selectByPatentExample(PatentExample example) {
        return patentSupplyMapper.selectByPatentExample(example);
    }

    @Override
    public List<PatentSupply> getAll() {
        return patentSupplyMapper.getAll();
    }

    // ========================重构为异步任务================================

    @Async
    @Override
    public void asyncTask(AbstractTask task) throws TaskException {
        task.execute();
    }

    @Override
    public String getProcess(Long taskId) {
        if (taskId == null) {
            return "0";
        }
        Cache cache = springCacheManager.getCache(EhCacheKeys.VERIFICATION_CODE_CACHE);
        if (cache.get(taskId) == null) {
            return "0";
        }
        String progress = cache.get(taskId).get().toString();
        logger.debug("获取task {} 进度：{}", taskId, progress);
        return progress;
    }

    @Override
    public void clearCache(Long taskId) {
        Cache cache = springCacheManager.getCache(EhCacheKeys.VERIFICATION_CODE_CACHE);
        logger.debug("task {} 进度完成，清除缓存", taskId);
        cache.evict(taskId);
    }
}

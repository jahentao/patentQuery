package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.constant.EhCacheKeys;
import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.mapper.PatentSupplyQueryMapper;
import com.jahentao.patentQuery.model.PatentSupplyQuery;
import com.jahentao.patentQuery.model.PatentSupplyQueryExample;
import com.jahentao.patentQuery.model.progress.AbstractTask;
import com.jahentao.patentQuery.service.PatentSupplyQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/30
 * @since 1.0
 */
@Service
public class PatentSupplyQueryServiceImpl extends BaseServiceImpl  implements PatentSupplyQueryService {

    @Autowired
    private PatentSupplyQueryMapper patentSupplyQueryMapper;
    @Resource
    private CacheManager springCacheManager;

    @Override
    public PatentSupplyQuery selectByKey(Long key) {
        return patentSupplyQueryMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(PatentSupplyQuery entity) {
        return patentSupplyQueryMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentSupplyQueryMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(PatentSupplyQuery entity) {
        return patentSupplyQueryMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(PatentSupplyQuery entity) {
        return patentSupplyQueryMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<PatentSupplyQuery> selectByExample(Object example) {
        return patentSupplyQueryMapper.selectByExample((PatentSupplyQueryExample) example);
    }

    ///////////////// 执行转换任务时，提供的服务

    @Async
    @Override
    public void asyncTask(AbstractTask task) throws TaskException {
        task.execute();
    }

    @Override
    public String getProcess(Long taskId) {
        if (taskId == null) {
            return "0";
        }
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_SUPPLY_QUERY_IMPORT_TO_DEMAND_TASK_CACHE);
        if (cache.get(taskId) == null) {
            return "0";
        }
        String progress = cache.get(taskId).get().toString();
        logger.debug("获取task {} 进度：{}", taskId, progress);
        return progress;
    }

    @Override
    public void clearCache(Long taskId) {
        Cache cache = springCacheManager.getCache(EhCacheKeys.ASYNC_SUPPLY_QUERY_IMPORT_TO_DEMAND_TASK_CACHE);
        logger.debug("task {} 进度完成，清除缓存", taskId);
        cache.evict(taskId);
    }
}

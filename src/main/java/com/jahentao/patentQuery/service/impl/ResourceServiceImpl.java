package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.ResourceMapper;
import com.jahentao.patentQuery.model.Resource;
import com.jahentao.patentQuery.model.ResourceExample;
import com.jahentao.patentQuery.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/5/26
 * @since 1.0
 */
@Service
public class ResourceServiceImpl extends BaseServiceImpl  implements ResourceService {

    @Autowired
    private ResourceMapper resourceMapper;

    @Override
    public Resource selectByKey(Long key) {
        return resourceMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(Resource entity) {
        return resourceMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return resourceMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(Resource entity) {
        return resourceMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(Resource entity) {
        return resourceMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<Resource> selectByExample(Object example) {
        return resourceMapper.selectByExample((ResourceExample) example);
    }
}

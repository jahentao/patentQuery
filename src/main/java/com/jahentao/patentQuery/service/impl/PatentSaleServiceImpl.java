package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.PatentSaleMapper;
import com.jahentao.patentQuery.model.PatentSale;
import com.jahentao.patentQuery.model.PatentSaleExample;
import com.jahentao.patentQuery.service.PatentSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
@Service
public class PatentSaleServiceImpl extends BaseServiceImpl  implements PatentSaleService {

    @Autowired
    private PatentSaleMapper patentSaleMapper;

    @Override
    public PatentSale selectByKey(Long key) {
        return patentSaleMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(PatentSale entity) {
        return patentSaleMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return patentSaleMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(PatentSale entity) {
        return patentSaleMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(PatentSale entity) {
        return patentSaleMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<PatentSale> selectByExample(Object example) {
        return patentSaleMapper.selectByExample((PatentSaleExample) example);
    }
}

package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.AdminMapper;
import com.jahentao.patentQuery.model.Admin;
import com.jahentao.patentQuery.model.AdminExample;
import com.jahentao.patentQuery.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/15
 * @since 1.0
 */
@Service
public class AdminServiceImpl extends BaseServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin selectByKey(Long key) {
        return adminMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(Admin entity) {
        return adminMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return adminMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(Admin entity) {
        return adminMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(Admin entity) {
        return adminMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<Admin> selectByExample(Object example) {
        return adminMapper.selectByExample((AdminExample) example);
    }

    @Override
    public Admin login(Admin admin) {
        return adminMapper.login(admin);
    }
}

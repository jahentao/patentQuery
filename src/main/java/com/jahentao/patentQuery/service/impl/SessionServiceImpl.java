package com.jahentao.patentQuery.service.impl;

import com.jahentao.patentQuery.mapper.SessionMapper;
import com.jahentao.patentQuery.model.Session;
import com.jahentao.patentQuery.model.SessionExample;
import com.jahentao.patentQuery.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/5/26
 * @since 1.0
 */
@Service
public class SessionServiceImpl extends BaseServiceImpl  implements SessionService {

    @Autowired
    private SessionMapper sessionMapper;

    @Override
    public Session selectByKey(Long key) {
        return sessionMapper.selectByPrimaryKey(key);
    }

    @Override
    public Integer save(Session entity) {
        return sessionMapper.insert(entity);
    }

    @Override
    public Integer delete(Long key) {
        return sessionMapper.deleteByPrimaryKey(key);
    }

    @Override
    public Integer updateAll(Session entity) {
        return sessionMapper.updateByPrimaryKey(entity);
    }

    @Override
    public Integer updateNotNull(Session entity) {
        return sessionMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public List<Session> selectByExample(Object example) {
        return sessionMapper.selectByExample((SessionExample) example);
    }
}

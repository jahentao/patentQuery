package com.jahentao.patentQuery.service.impl;

import com.aliyun.mns.common.ServiceException;
import com.jahentao.patentQuery.service.RegisterValidateService;
import org.springframework.stereotype.Service;

/**
 * @author jahentao
 * @date 2018/6/11
 * @since 1.0
 */
@Service
public class RegisterValidateServiceImpl implements RegisterValidateService {

    @Override
    public void processRegister(String email){
        ///如果处于安全，可以将激活码处理的更复杂点

        //保存注册信息

        ///邮件的内容

        //发送邮件

    }

    /**
     * 处理激活
     * <p>传递激活码和email过来</p>
     */
    @Override
    public void processActivate(String email, String validateCode){
        //数据访问层，通过email获取用户信息

        //验证用户是否存在

    }
}

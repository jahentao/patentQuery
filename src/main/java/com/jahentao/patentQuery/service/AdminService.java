package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.Admin;

/**
 * @author jahentao
 * @date 2018/4/15
 * @since 1.0
 */
public interface AdminService extends IService<Admin> {
    Admin login(Admin admin);
}

package com.jahentao.patentQuery.service;

import java.util.List;

/**
 * Created by jahentao on 2018/1/23.
 */
public interface IService<T> {
    /**
     * 根据主键查询
     * @param key
     * @return
     */
    T selectByKey(Long key);

    /**
     * 持久化实体<br>
     *
     * 插入实体的外键引用必须存在，同时在测试或开发等逻辑中，不允许出现外键为空的情形！
     * @param entity
     * @return
     */
    Integer save(T entity);

    /**
     * 根据主键删除
     * @param key
     * @return
     */
    Integer delete(Long key);

    /**
     * 更新实体所有属性，根据主键查询实体
     * @param entity
     * @return
     */
    Integer updateAll(T entity);

    /**
     * 更新实体非空属性，根据主键查询实体
     * @param entity
     * @return
     */
    Integer updateNotNull(T entity);

    /**
     * 根据Example查询，系统开发人员需要根据需求，自主分页
     * @param example
     * @return
     */
    List<T> selectByExample(Object example);

}

package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.model.progress.AbstractTask;

/**
 * @author jahentao
 * @date 2018/6/2
 * @since 1.0
 */
public interface AsyncService {
    /**
     * 异步执行任务
     * <p>中间进度放入缓存中</p>
     * @param task
     * @throws TaskException
     */
    void asyncTask(AbstractTask task) throws TaskException;

    /**
     * 获取任务进度
     * <p>从缓存中中获取任务进度</p>
     * @param taskId
     * @return
     * @throws Exception
     */
    String getProcess(Long taskId);

    /**
     * 清除缓存
     * @param taskId
     * @throws Exception
     */
    void clearCache(Long taskId);
}

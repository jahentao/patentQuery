package com.jahentao.patentQuery.service;

/**
 * @author jahentao
 * @date 2018/6/11
 * @since 1.0
 */
public interface RegisterValidateService {

    /**
     * 处理注册
     */
    void processRegister(String email);
    /**
     * 处理激活
     * <p>传递激活码和email过来</p>
     * @param email 邮箱
     * @param validateCode 验证码
     */
    void processActivate(String email, String validateCode);

}

package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.User;

import java.util.List;
import java.util.Set;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
public interface UserService extends IService<User> {
    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 根据用户名查找用户角色
     * @param username
     * @return
     */
    Set<String> findRoles(String username);

    /**
     * 根据用户名查找用户权限
     * @param username
     * @return
     */
    Set<String> findPermissions(String username);

    /**
     * 获取注册的用户
     * @return
     */
    List<User> getRegisteredUser();

    /**
     * 获取QQ登录的游客
     * @return
     */
    List<User> getVisitor();
}

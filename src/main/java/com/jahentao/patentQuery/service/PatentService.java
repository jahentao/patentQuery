package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.Patent;

/**
 * 专利提供的服务
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
public interface PatentService extends IService<Patent>{
    // 幸好中国的专利号是整数，长整型可以表示，IService接口还能继承。要不然selectByKey()，deleteByKey()就要调整了
}

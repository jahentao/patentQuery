package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.User;

/**
 * Created by jahentao on 2018/2/18.<br>
 * Version: 1.0
 * <p>处理登录、注册、注销、第三方登录等功能</p>
 */
public interface UserLoginService {

    /**
     * 用户登录
     * @param user
     * @param rememberMe
     * @param host
     */
    void loginWithUsername(User user, Boolean rememberMe, String host);

    /**
     * 退出
     * @param user
     */
    void logout(User user);
}

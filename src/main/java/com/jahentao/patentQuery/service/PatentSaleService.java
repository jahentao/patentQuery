package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.PatentSale;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
public interface PatentSaleService extends IService<PatentSale> {
}

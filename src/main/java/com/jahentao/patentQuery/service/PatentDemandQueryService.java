package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.exception.TaskException;
import com.jahentao.patentQuery.model.PatentDemandQuery;
import com.jahentao.patentQuery.model.progress.AbstractTask;

/**
 * @author jahentao
 * @date 2018/4/30
 * @since 1.0
 */
public interface PatentDemandQueryService extends IService<PatentDemandQuery>, AsyncService {

}

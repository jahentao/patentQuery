package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.PatentDemand;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
public interface PatentDemandService extends IService<PatentDemand>, AsyncService {
    List<PatentDemand> getAll();
}

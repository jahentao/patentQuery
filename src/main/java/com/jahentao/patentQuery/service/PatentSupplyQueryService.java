package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.PatentSupplyQuery;

/**
 * @author jahentao
 * @date 2018/4/30
 * @since 1.0
 */
public interface PatentSupplyQueryService extends IService<PatentSupplyQuery>, AsyncService {
}

package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.Session;

/**
 * @author jahentao
 * @date 2018/5/26
 * @since 1.0
 */
public interface SessionService extends IService<Session> {
}

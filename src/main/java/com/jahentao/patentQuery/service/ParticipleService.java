package com.jahentao.patentQuery.service;

import org.ansj.domain.Result;

/**
 * 分词服务
 * @author jahentao
 * @date 2018/4/14
 * @since 1.0
 */
public interface ParticipleService {

    /**
     * 分词
     * @param str 字符串内容
     * @return 分词结果集
     */
    Result parse(String str);

    //////////////操作词典




    //////////////将词典写入响应文件


}

package com.jahentao.patentQuery.service;

import com.jahentao.patentQuery.model.PatentExample;
import com.jahentao.patentQuery.model.PatentSupply;

import java.util.List;

/**
 * @author jahentao
 * @date 2018/4/13
 * @since 1.0
 */
public interface PatentSupplyService extends IService<PatentSupply>, AsyncService {
    List<PatentSupply> selectByPatentExample(PatentExample example);

    List<PatentSupply> getAll();
}

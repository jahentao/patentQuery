package com.jahentao.patentQuery.util.mail;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dm.model.v20151123.SingleSendMailRequest;
import com.aliyuncs.dm.model.v20151123.SingleSendMailResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * @author jahentao
 * @date 2018/6/4
 * @since 1.0
 */
public class SendMailUtil {
    /**
     * 给指定地址发送注册验证码的邮件
     * @param toAddress 接收地址
     * @param code 随机生成验证码
     */
    public static void sendVerificationCodeInEmail(String toAddress, String code) throws ClientException {
        String subject = "【知万千】注册验证码";
        StringBuilder htmlBody = new StringBuilder("<div id=\"mailContentContainer\" class=\"qmbox qm_con_body_content qqmail_webmail_only\" style=\"\"><style type=\"text/css\">.qmbox body {color: #333;}.qmbox a {color: #47ba83;}.qmbox p {text-align: center;}.qmbox #box {width: 600px;margin: 20px auto;padding: 15px;line-height: 1.4em;background: #4c9fb3;}.qmbox #box a {color: #333333;text-decoration: none;}</style><div id=\"box\"><p><a href=\"http://www.zhiwanqian.com\" target=\"_blank\"><img src=\"http://www.zhiwanqian.com/patentQuery//images/logo.png\"style=\"width:160px; height:34px;\"></a></p><p style=\"font-size: 12px;color:#ffffff; margin-bottom: 36px; margin-top: 15px;\">南京启兴知识产权服务有限公司</p><p style=\"color:#333333; font-size: 24px; margin-bottom: 15px;\">尊敬的<a href=\"mailto:");
        htmlBody.append(toAddress);
        htmlBody.append("\">");
        htmlBody.append(toAddress);
        htmlBody.append("</a></p><p style=\"margin-bottom: 37px;\">您好，您刚注册成为南京启兴知识产权服务有限公司旗下「知万千」信息找寻平台的用户，您注册的邮件验证码为</p><p><a style=\"display:inline-block; font-size: 14px; height: 30px;width:120px; color:#ffffff !important; border: solid 1px; border-radius: 4px; line-height: 30px;\">");
        htmlBody.append(code);
        htmlBody.append("</a></p>");
        htmlBody.append("<p style=\"font-size: 12px;color:#eadcdc;\">验证码5分钟后失效，感谢您的操作，祝您使用愉快！</p></div><p style=\"font-size: 12px;color:#999999;margin:20px auto; width:600px;\">本邮件由「知万千」邮件系统自动发送，请勿直接回复！在使用平台过程中若有遇到任何疑问，请联系管理员。</p><style type=\"text/css\">.qmbox style, .qmbox script, .qmbox head, .qmbox link, .qmbox meta {display: none !important;}</style></div>");
        sendEmail(toAddress,subject, htmlBody.toString());
    }

    /**
     * 发送邮件
     * <p>使用阿里云邮件推送服务，发送单个邮件</p>
     * @param toAddress 接收邮件的地址
     * @param subject 邮件主题
     * @param htmlBody 邮件内容（HTML 格式）
     */
    public static void sendEmail(String toAddress, String subject, String htmlBody) throws ClientException {
        // 如果是除杭州region外的其它region（如新加坡、澳洲Region），需要将下面的"cn-hangzhou"替换为"ap-southeast-1"、或"ap-southeast-2"。
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAIoDbsiUvbxdj4", "fXudrZmbrrn3aQqfANHfIS2hZmRqte");
        try {
            DefaultProfile.addEndpoint("dm.aliyuncs.com", "cn-hangzhou", "Dm", "dm.aliyuncs.com");
        } catch (ClientException e) {
            e.printStackTrace();
        }
        IAcsClient client = new DefaultAcsClient(profile);
        SingleSendMailRequest request = new SingleSendMailRequest();
        try {
            //request.setVersion("2017-06-22");// 如果是除杭州region外的其它region（如新加坡region）,必须指定为2017-06-22
            request.setAccountName("admin@email.zhiwanqian.com");
            request.setFromAlias("知万千");
            request.setAddressType(1);
            request.setTagName("register");
            request.setReplyToAddress(true);
            request.setToAddress(toAddress);
            request.setSubject(subject);
            request.setHtmlBody(htmlBody);
            SingleSendMailResponse httpResponse = client.getAcsResponse(request);
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }

}

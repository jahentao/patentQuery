package com.jahentao.patentQuery.util.themeleaf;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * 将原HTML转换为Themeleaf的模板工具类
 * <p>采用<tt>jsoup</tt>解析，而非<tt>dom4j</tt>解析</p>
 * @author jahentao
 * @date 2018/4/20
 * @since 1.0
 */
public class ThemeleafTemplateUtil {

    /**
     * 解析HTML文件为符合Themeleaf的模板
     * @param htmlFile 待转换的html文件
     */
    public static void parse(File htmlFile, String suffix, String fixPath, String fixThemeleafPath) throws IOException {
        Document doc = Jsoup.parse(htmlFile, "UTF-8", "http://zhiwanqian.com/patentQuery/");

        Elements htmlElement = doc.getElementsByTag("html");
//        System.out.println(htmlElements.size()); // 只能获得一个html的标签

        // 1. 在html上添加xmlns命名空间
        htmlElement.attr("xmlns:th","http://www.thymeleaf.org");

        // 2. 处理 link 标签
        handleSpecialTag("link", "href", "th:href", doc, htmlFile, suffix, fixPath, fixThemeleafPath);

        // 3. 处理 script标签
        handleSpecialTag("script", "src", "th:src", doc, htmlFile, suffix, fixPath, fixThemeleafPath);

        // 4. 处理 img 标签
        handleSpecialTag("img", "src", "th:src", doc, htmlFile, suffix, fixPath, fixThemeleafPath);

        // 5. 文件写出
        // Jsoup只是解析，不能保存修改，所以要在这里保存修改。
        FileOutputStream fos = new FileOutputStream(htmlFile, false);
        OutputStreamWriter osw = new OutputStreamWriter(fos, "utf-8");
        osw.write(doc.html());
        osw.close();
    }

    /**
     * 处理特定的标签
     * <p>e.g. 在 htmlFile <pre>F:\Workspace\idea\patentQuery\src\main\webapp\WEB-INF\admin\demandQuery\list.html</pre>中
     * 有js的引入：<pre>href="/patentQuery/js/jquery.min.js"</pre>
     * 对应的资源文件在 <pre>F:\Workspace\idea\patentQuery\src\main\webapp\js\jquery.min.js</pre>
     * 判断如下：
     * <ul>
     *     <li>有前缀suffix：<pre>/patentQuery/</pre>，就是<pre>js/jquery.min.js</pre>前的都是前缀</li>
     *     <li>从htmlFile到资源文件真正的路径，所需要修补的路径fixPath：<pre>../../../</pre>，即当前在<pre>F:\Workspace\idea\patentQuery\src\main\webapp\WEB-INF\admin\demandQuery\</pre>目录下
     *     ，到<pre>F:\Workspace\idea\patentQuery\src\main\webapp\</pre>需要补上的路径为<pre>../../../</pre>，然后<pre>F:\Workspace\idea\patentQuery\src\main\webapp\</pre>加上<pre>js/jquery.min.js</pre>
     *     即可得到资源文件</li>
     *     <li>Themeleaf请求响应的web资源路径fixThemeleafPath：<pre>/</pre>，即通过<pre>/js/jquery.min.js</pre>可请求到资源文件</li>
     * </ul>
     * </p>
     * @param tagName
     * @param tagAttrName
     * @param addAttrName
     * @param doc
     * @param htmlFile
     * @param fixPath
     * @param fixThemeleafPath
     */
    private static void handleSpecialTag(String tagName, String tagAttrName, String addAttrName,
                                         Document doc, File htmlFile, String suffix, String fixPath, String fixThemeleafPath) {
        Elements links = doc.getElementsByTag(tagName);
        for (Element link : links) {
//            System.out.println("===============================");
            String attr_th_href = link.attr(addAttrName);
            // 如果 有 th:href 属性了，默认被用户修改，且默认正确
            if (StringUtils.isEmpty(attr_th_href)) {
                String attr_href = link.attr(tagAttrName).replace(suffix, "");
                if (!StringUtils.isEmpty(attr_href)) {
                    String[] split = attr_href.split("\\?");
                    String fileName = split[0];
//                    System.out.println(fileName);
                    // 判断相对位置的文件是否存在
                    File file = new File(htmlFile.getParent()+File.separator+fileName);
                    // 文件不存在，则修复相对路径问题
                    if (!file.exists()) {
                        // 如何修复，根据用户的fixPath前缀
                        String fixedFilePath = htmlFile.getParent()+File.separator+fixPath+fileName;
//                        System.out.println(fixedFilePath);
                        File fixedFile = new File(fixedFilePath);
                        // 如果修复后的文件存在那就修改href，否则，就不改了，维持原样
                        if (fixedFile.exists()) {
//                            System.out.println(fixedFile.getAbsolutePath());
//                            System.out.println(fixPath+fileName);
                            link.attr(tagAttrName,fixPath+attr_href);
                            // 添加 th:href 属性
                            link.attr(addAttrName,"@{"+fixThemeleafPath+attr_href+"}");
                        }
                    }
                }
//                System.out.println(link);
            }
        }
    }
}

package com.jahentao.patentQuery.realm;

import com.jahentao.patentQuery.model.User;
import com.jahentao.patentQuery.service.UserService;
import com.jahentao.patentQuery.util.PasswordHelper;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by jahentao on 2018/2/9.<br>
 * Version: 1.0<br>
 * 获取用户信息的安全数据源<br>
 * userRealm组件禁用掉了cache；需要在修改如资源/角色等信息时清理掉缓存
 */
@Component
public class UserRealm extends AuthorizingRealm {
    // 问题：不是在Controller类,所以使用@Autowired注解说明这个service类是没有用的，无法获取这个类，这个类还是null
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordHelper passwordHelper;

    public static UserRealm userRealm;

    // 解决方法，参考：http://blog.csdn.net/qq_35056292/article/details/78430777
    @PostConstruct
    public void init0() {
        userRealm = this;
        userRealm.userService = this.userService;
        userRealm.passwordHelper = this.passwordHelper;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String)principals.getPrimaryPrincipal();

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(userRealm.userService.findRoles(username));
        authorizationInfo.setStringPermissions(userRealm.userService.findPermissions(username));
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String username = (String)token.getPrincipal();

        User user = userRealm.userService.findByUsername(username); // 这里的User需要使用到密码

        if(user == null) {
            throw new UnknownAccountException();//没找到帐号
        }

        if(Boolean.TRUE.equals(user.getLocked())) {
            throw new LockedAccountException(); //帐号锁定
        }

        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以自定义实现
        // 在本应用patentQuery中，采用随机生成salt作为盐值，即不使用用户定义的盐值
        String credentialsSalt = userRealm.passwordHelper.getUseDefinitedSalt() ? userRealm.passwordHelper.getSalt() : user.getSalt();

        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user.getUsername(), //用户名
                user.getPassword(), //密码
                ByteSource.Util.bytes(credentialsSalt),
                getName()  //realm name
        );
        return authenticationInfo;
    }

    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }

    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

}

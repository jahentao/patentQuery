$(document).ready(function () {
    //分页查询显示
    $('#page').bPage({
        url: '/patentQuery/admin/getList',
        //开启异步处理模式
        asyncLoad: true,
        //关闭服务端页面模式
        serverSidePage: false,
        //数据自定义填充
        render: function (returnData) {
            // table 容器
            $('#adminShow').html('' +
                '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '     <thead>\n' +
                '     <tr>\n' +
                '         <th class="selectColumn" >选择</th>\n' +
                '         <th>姓名</th>\n' +
                '         <th>密码</th>\n' +
                '         <th>邮箱</th>\n' +
                // '         <th>性别</th>\n' +
                '     </tr>\n' +
                '     </thead>\n' +
                '     <tbody>\n' +
                '     </tbody>\n' +
                '</table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            console.log(returnData);

            // 有数据库时填充数据库
            if (returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0) {
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function (i, row) {
                    var tr = $('<tr>');
                    $(tr).append('<td align="center"><input type="checkbox"></td>');
                    $(tr).append('<td>' + row.name + '</td>');
                    $(tr).append('<td>' + row.password + '</td>');
                    $(tr).append('<td>' + row.email + '</td>');
                    // $(tr).append('<td>' + row.sex == 1 ? '男' : '女' + '</td>');
                    $(tb).append(tr);
                });
            } else { // 没有数据显示提示
            }
        }
    });
});
/**
 * 基于layer提示信息，校验功能的简易jQuery插件
 * 现在只进行require的支持
 * @author jahentao
 * @since 1.0
 */
;(function($){
    /**
     * 定义 Validator 的构造函数
     * @param ele 传入表单的$对象
     * @param opt 参数
     * @constructor
     */
    var Validator = function (ele, opt) {
        this.$element = ele;
        this.defaults = {}; // 暂时想不到默认参数需要什么
        this.options = $.extend({}, this.defaults, opt);
    };
    //定义 Validator 的方法
    Validator.prototype = {
        /**
         * 校验
         */
        validate: function() {
            // console.log("hello world");
            // 监听表单含require的input框
            $('input[required]').each(function () {
                $that = $(this);
                var trigger =  function (e) {
                    var $input =  $(e.currentTarget);
                    var value = $input.val();
                    // 正则表达式匹配
                    var reg = new RegExp(e.currentTarget.pattern,'g');
                    if (value === "" || !reg.test(value)) {
                        // data-tip 和 title 至少有一个吧
                        var msg = e.currentTarget.dataset.tip !== undefined ? e.currentTarget.dataset.tip : e.currentTarget.title;
                        layer.tips(msg, e.currentTarget, {
                            tips: 2 // 右边
                        });
                    }
                };
                // $that.on('change', trigger);
                // 失去焦点，如Tab键。与change重复了，会触发两次的。直接用blur
                $that.blur(trigger);
            });

        }
    };

    //在插件中使用 Validator 对象
    $.fn.validate = function(options) {
        //创建 Validator 的实例
        var validator = new Validator(this, options);
        //调用其方法
        return validator.validate();
    }

})(jQuery);

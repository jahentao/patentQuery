// getList 分页显示
$(document).ready(function () {
    // 分页查询显示
    $('#page').bPage({
        url: '/patentQuery/demandQuery/getList',
        //开启异步处理模式
        asyncLoad: true,
        //关闭服务端页面模式
        serverSidePage: false,
        //数据自定义填充
        render: function (returnData) {
            // table 容器
            $('#demandQueryShow').html('' +
                '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '     <thead>\n' +
                '     <tr>\n' +
                '         <th class="selectColumn" >选择</th>\n' +
                '         <th>专利号</th>\n' +
                '         <th>专利名称</th>\n' +
                '         <th>用户联系方式</th>\n' +
                '         <th>搜索时间</th>\n' +
                '     </tr>\n' +
                '     </thead>\n' +
                '     <tbody>\n' +
                '     </tbody>\n' +
                '</table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            console.log(returnData);

            // 有数据库时填充数据库
            if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function(i,row){
                    var tr = $('<tr>');
                    $(tr).append('<td align="center"><input type="checkbox" data-id="'+row.id+'"></td>');
                    $(tr).append('<td>'+row.patentId+'</td>');/*专利号*/
                    $(tr).append('<td>'+row.name+'</td>');/*专利名称*/
                    $(tr).append('<td>'+getAssociationOfUser(row.user)+'</td>');/*搜索用户 联系号码*/
                    $(tr).append('<td>'+timestampToTime(row.queryTime)+'</td>');/*搜索时间*/
                    $(tb).append(tr);
                });
            } else { // 没有数据显示提示
            }
        }
    });

    // 添加按钮事件
    // 全部选中
    $('#checkall').click(function () {
        var checked = !($('#checkall').data('checked')==='true');

        //查找tbody中所有第一个td下的input
        var chbs=document.querySelectorAll(
            "tbody td:first-child>input"
        );
        //遍历chbs中每个chb
        for(var i=0;i<chbs.length;i++){
            //设置当前chb的checked等于this的checked
            chbs[i].checked=checked;
        }
        $('#checkall').data('checked',checked.toString());
    });
});

// 批量删除
$('#dels').click(function () {

    //查找tbody中所有第一个td下的input
    var chbs=document.querySelectorAll(
        "tbody td:first-child>input"
    );

    // 存放待删除id
    var ids = [];

    //遍历chbs中每个chb
    for(var i=0;i<chbs.length;i++){
        if (chbs[i].checked===true){
            ids.push($(chbs[i]).data('id'))
        }
    }

    $.ajax({
        url : '/patentQuery/demandQuery/deleteIds',
        type: 'POST',
        cache: false,
        dataType: 'json',
        timeout : 50000, //超时时间：50秒
        async : false,
        data: {ids : ids},
        success : function (returnData) {
            // 刷新当前页
            location.reload();
        }
    });
});

var demand_interval;
// 开始转换
function convertStart() {
    // 通知开始转换
    $.ajax({
        url:'/patentQuery/demandQuery/convert',
        type:'POST',
        async : false,
        dataType: 'json',
        data: {
            selectAll : $('#selectAll').prop('checked'),
            start : $("#start").val(),
            end : addDate($("#end").val(), 1)
        },
        success: function (returnData) {
            if (returnData.data && returnData.data.taskId) {
                $('#progress_wrap').data('taskId', returnData.data.taskId);

                // Ajax 轮询进度——"定时的通过Ajax查询服务端"
                // TODO 现在好像有 "长轮询"、"服务器端推"等其他技术方案
                demand_interval = setInterval(function () {
                    $.ajax({
                        url:'/patentQuery/demandQuery/getProgress',
                        type:'POST',
                        data: {
                            taskId : $('#progress_wrap').data('taskId')
                        },
                        dataType: 'json',
                        success: function (returnData) {
                            if (returnData.data && returnData.data.progress) {
                                $('#progress').text('进度:'+ returnData.data.progress + '%');
                                // 并存储到浏览器端
                                sessionStorage.setItem(returnData.data, returnData.data.progress);
                                $('#progress').width(returnData.data.progress + '%');
                                if (returnData.data.progress === '100.0') {
                                    clearInterval(demand_interval);
                                    sessionStorage.clear();
                                    // 启用按钮
                                    $('#convertBtn').attr('disabled',false);
                                }
                            }
                        },
                        error: function (returnData) {
                            console.log('获取进度信息失败');
                        }
                    });
                }, 1000);
            }
        },
        error: function (returnData) {
            alert('服务器异常，请稍后再试');
        }
    });

    // 禁用按钮
    $('#convertBtn').attr('disabled',true);
    $('#progress_wrap').show();
    $('#convertBtn').hide();

}

// 转换导入
$('#convert').click(function () {
    //页面层
    layer.open({
        type: 1,
        title: '转换配置',
        skin: 'layui-layer-rim', //加上边框
        area: ['420px', '240px'], //宽高
        content:
        '<div style="padding: 10px; line-height: 34px;">选择导入时间段：' +
        '    <div>' +
        '        <input type="date" id="start"> - ' +
        '        <input type="date" id="end">&nbsp;&nbsp;' +
        '        <input type="checkbox" id="selectAll"> 所有' +
        '    </div>' +
        '    <div>' +
        '        <button onclick="convertStart();" id="convertBtn" type="button" class="button border-blue" style="position:relative;left:38%;margin-top:10px;"><span class="icon-lightbulb-o"></span> 开始转换</button>' +
        '    </div>' +
        '    <div id="progress_wrap" style="width:100%;margin-top:40px;" hidden>' +
        '        <div class="progress">' +
        '            <div id="progress" class="progress-bar bg-blue" style="width: 0%;">进度：0%</div>' +
        '        </div>' +
        '    </div>' +
        '</div>',
        cancel: function () {
            clearInterval(demand_interval);
        }
    });

    set_date();

});
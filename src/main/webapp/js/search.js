// 注意从系统用户（工作人员）的角度去理解“供”和“求”

// 找需求 找买家
$("#demandForm").submit(function(e){
    set_date();

    $('#reSiftBtn').data('type','demand');

    // demand 分页显示
    demand();

    return false; // fix 用于解决表单提交时类似刷新页面的效果
});

// 搜索框绑定回车事件
$('#demandForm input').eq(0).bind('keypress',function(event){
    if(event.keyCode === "13") {
        $("#demandForm").submit();
    }
});

function demand() {

    $('#register').addClass('hidden');
    $('#sift').addClass('hidden');
    $('#message').addClass('hidden');

    $("#supplyForm input[type='text']").val('');

    var patentId = $("#demandForm input[type='text']").eq(0).val();
    var patentName = $("#demandForm input[type='text']").eq(1).val();
    
    if (patentId === "undefined" || patentId === "" || patentId === "请输入专利号") {
        alert("专利号不能为空");
        return;
    }

    if (patentName === "undefined" || patentName === "" || patentName === "请输入专利名称") {
        alert("专利名称不能为空");
        return;
    }

    //分页查询显示
    $('#page').bPage({
        url : 'demand/search',
        //开启异步处理模式
        asyncLoad : true,
        //关闭服务端页面模式
        serverSidePage : false,
        //数据自定义填充
        render : function(returnData){
            
            if (handleMessageCode(returnData) === false)
                return;
            
            // table 容器
            $('#patentQuery').html('' +
                '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '     <thead>\n' +
                '     <tr>\n' +
                // '         <th class="selectColumn" >选择</th>\n' +
                '         <th>求购内容</th>\n' +
                '         <th>QQ号</th>\n' +
                '         <th>时间</th>\n' +
                '     </tr>\n' +
                '     </thead>\n' +
                '     <tbody>\n' +
                '     </tbody>\n' +
                '</table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            console.log(returnData);

            $('#sift').removeClass("hidden");
            $('#patentType').addClass('hidden');
            $('#patentStatus').addClass('hidden');

            // 按客户要求，隐藏分词
            var participle = returnData.message.replace('[','').replace(']','').split(',');
            console.log(participle);
            // 填充分词
            $('#participle-content').empty();
            $.each(participle, function (i, row) {
                var li = $('<li>');
                $(li).addClass('list-group-item');
                $(li).append(row);
                $('#participle-content').append(li);
            });

            // 有数据库时填充数据库
            if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function(i,row){
                    var tr = $('<tr>');
                    // $(tr).append('<td></td>');
                    $(tr).append('<td>'+row.content+'</td>');/*求购内容*/
                    $(tr).append('<td>'+row.user.qq+'</td>');/*qq号*/
                    $(tr).append('<td>'+timestampToTime(row.demandTime)+'</td>');/*时间*/
                    $(tb).append(tr);
                });
            }else { // 没有数据显示提示
                $("#page").addClass("hidden");
                $('#message').removeClass('hidden');

                var id = $('#reSiftBtn').data('id');
                var name = $('#reSiftBtn').data('name');
                var message = "专利\"{";
                if (id !== "请输入专利号" && id !== "undefined" && id !== "") {
                    message += "id: "+id + "，";
                }
                if (name !== "请输入专利名称" && name !== "undefined" && name !== "") {
                    message += "name: "+name ;
                }else {
                    message.replace('，','');
                }
                message += "}\"";

                $('#message').html('没有'+message+'的相关求购信息');
            }
        },
        params : function(){
            return {
                id : patentId,
                name : patentName,
                selectAll : $('#selectAll').prop('checked'),
                start : $("#start").val(),
                end : $("#end").val()
            };
        }
    });

    $('#reSiftBtn').data('id',patentId);
    $('#reSiftBtn').data('name',patentName);
}

// 找专利 找卖家
$("#supplyForm").submit(function(e){
    set_date();
    $('#patentType select').val('0');
    $('#patentStatus select').val('0');

    $('#reSiftBtn').data('type','supply');

    // supply 分页显示
    supply();

    return false;// fix 用于解决表单提交时类似刷新页面的效果
});

// 搜索框绑定回车事件
$('#supplyForm input').eq(0).bind('keypress',function(event){
    if(event.keyCode === "13") {
        $("#supplyForm").submit();
    }
});

function supply() {

    $('#register').addClass('hidden');
    $('#sift').addClass('hidden');
    $('#message').addClass('hidden');

    $("#demandForm input[type='text']").eq(0).val('');
    $("#demandForm input[type='text']").eq(1).val('');

    var key = $("#supplyForm input[type='text']").val();

    if (key === "undefined" || key === "" || key === "求购制冷设备相关的未缴费发明专利，请输入“制冷设备”") {
        alert("请输入查询关键词");
        return;
    }

    //分页查询显示
    $('#page').bPage({
        url : 'supply/search',
        //开启异步处理模式
        asyncLoad : true,
        //关闭服务端页面模式
        serverSidePage : false,
        //数据自定义填充
        render : function(returnData){

            if (handleMessageCode(returnData) === false)
                return;

            // table 容器
            $('#patentQuery').html('<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '                    <thead>\n' +
                '                    <tr>\n' +
                // '                        <th class="selectColumn" >选择</th>\n' +
                '                        <th>专利号</th>\n' +
                '                        <th>专利名称</th>\n' +
                '                        <th>专利类型</th>\n' +
                '                        <th>专利状态</th>\n' +
                '                        <th>QQ号</th>\n' +
                '                        <th>时间</th>\n' +
                '                    </tr>\n' +
                '                    </thead>\n' +
                '                    <tbody>\n' +
                '                    </tbody>\n' +
                '                </table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            console.log(returnData);

            $('#sift').removeClass("hidden");
            $('#patentType').removeClass("hidden");
            $('#patentStatus').removeClass("hidden");

            // 按客户要求，隐藏分词
            var participle = returnData.message.replace('[','').replace(']','').split(',');
            console.log(participle);
            // 填充分词
            $('#participle-content').empty();
            $.each(participle, function (i, row) {
                var li = $('<li>');
                $(li).addClass('list-group-item');
                $(li).append(row);
                $('#participle-content').append(li);
            });

            // 有数据库时填充数据库
            if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function(i,row) {
                    var tr = $('<tr>');
                    // $(tr).append('<td></td>');
                    $(tr).append('<td>'+row.patent.id+'</td>');/*专利号*/
                    $(tr).append('<td>'+row.patent.name+'</td>');/*专利名称*/
                    $(tr).append('<td>'+indexToPatentType(row.patent.type)+'</td>');/*专利类型*/
                    $(tr).append('<td>'+indexToPatentStatus(row.patent.lawStatus)+'</td>');/*专利状态*/
                    $(tr).append('<td>'+row.user.qq+'</td>');/*qq号*/
                    $(tr).append('<td>'+timestampToTime(row.supplyTime)+'</td>');/*时间*/
                    $(tb).append(tr);
                });
            }else { // 没有数据显示提示
                $("#page").addClass("hidden");
                $('#message').removeClass('hidden');
                $('#message').html('没有“'+$('#reSiftBtn').data('key')+'”的相关专利信息');
            }
        },
        params : function(){
            return {
                key : key,
                patentType: $('#patentType select').val(),
                patentStatus: $('#patentStatus select').val(),
                selectAll : $('#selectAll').prop('checked'),
                start : $("#start").val(),
                end : $("#end").val()
            };
        }
    });

    $('#reSiftBtn').data('key',key);
}

// 重新筛选
$('#reSiftBtn').click(function () {
    var type = $('#reSiftBtn').data('type');
    if (type === 'demand')
        demand();
    else if (type === 'supply')
        supply();
});



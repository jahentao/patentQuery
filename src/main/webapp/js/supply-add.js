// addDemand 新增
$('#newDemandForm').submit(function () {
    $.ajax({
        url: '/patentQuery/supply/addSupply',
        type: 'POST',
        cache: false,
        dataType: 'json',
        timeout : 50000, //超时时间：50秒
        async : false,
        data: $('#newDemandForm').serialize(),
        error:function(data, XMLHttpRequest, textStatus, errorThrown){
            console.log(data);
            console.log(XMLHttpRequest.status);
            console.log(XMLHttpRequest.readyState);
            console.log(textStatus);
        },
        success : function (returnData) {
            handleMessageCode(returnData);
            // 保存成功，返回页面
            history.go(-1);
            // location.href="/patentQuery/supply/list";
        }
    });
});
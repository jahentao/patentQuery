// getList 分页显示
$(document).ready(function () {
    // 分页查询显示
    $('#page').bPage({
        url: '/patentQuery/user/getList',
        //开启异步处理模式
        asyncLoad: true,
        //关闭服务端页面模式
        serverSidePage: false,
        //数据自定义填充
        render: function (returnData) {
            // table 容器
            $('#userShow').html('' +
                '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '     <thead>\n' +
                '     <tr>\n' +
                '         <th class="selectColumn" >选择</th>\n' +
                '         <th>用户名</th>\n' +
                '         <th>手机号</th>\n' +
                '         <th>邮箱</th>\n' +
                '         <th>QQ</th>\n' +
                '         <th>是否锁定</th>\n' +
                '         <th>是否验证</th>\n' +
                '     </tr>\n' +
                '     </thead>\n' +
                '     <tbody>\n' +
                '     </tbody>\n' +
                '</table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            // console.log(returnData);

            // 有数据库时填充数据库
            if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function(i,row){
                    var tr = $('<tr>');
                    $(tr).append('<td align="center"><input type="checkbox" data-id="'+row.id+'"></td>');
                    $(tr).append('<td>'+row.username+'</td>');/*用户名*/
                    $(tr).append('<td>'+(row.telephone === null ? "" : row.telephone)+'</td>');/*手机号*/
                    $(tr).append('<td>'+(row.email === null ? "" : row.email)+'</td>');/*邮箱*/
                    $(tr).append('<td>'+(row.qq === null ? "" : row.qq)+'</td>');/*QQ*/
                    $(tr).append('<td>'+(row.locked === 1)+'</td>');/*是否锁定*/
                    $(tr).append('<td>'+(row.validated === 1)+'</td>');/*是否验证*/
                    $(tb).append(tr);
                });
            } else { // 没有数据显示提示
            }
        }
    });

    // 添加按钮事件
    // 全部选中
    $('#checkall').click(function () {
        var checked = !($('#checkall').data('checked')==='true');

        //查找tbody中所有第一个td下的input
        var chbs=document.querySelectorAll(
            "tbody td:first-child>input"
        );
        //遍历chbs中每个chb
        for(var i=0;i<chbs.length;i++){
            //设置当前chb的checked等于this的checked
            chbs[i].checked=checked;
        }
        $('#checkall').data('checked',checked.toString());
    });
});


// 批量删除
$('#dels').click(function () {

    //查找tbody中所有第一个td下的input
    var chbs=document.querySelectorAll(
        "tbody td:first-child>input"
    );

    // 存放待删除id
    var ids = [];

    //遍历chbs中每个chb
    for(var i=0;i<chbs.length;i++){
        if (chbs[i].checked===true){
            ids.push($(chbs[i]).data('id'))
        }
    }

    $.ajax({
        url : '/patentQuery/user/deleteIds',
        type: 'POST',
        cache: false,
        dataType: 'json',
        timeout : 50000, //超时时间：50秒
        async : false,
        data: {ids : ids},
        success : function (returnData) {
            // 刷新当前页
            location.reload();
        }
    });
});
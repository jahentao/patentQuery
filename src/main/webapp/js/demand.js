// getList 分页显示
$(document).ready(function () {
    // 分页查询显示
    $('#page').bPage({
        url: '/patentQuery/demand/getList',
        //开启异步处理模式
        asyncLoad: true,
        //关闭服务端页面模式
        serverSidePage: false,
        //数据自定义填充
        render: function (returnData) {
            // table 容器
            $('#demandShow').html('' +
                '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                '     <thead>\n' +
                '     <tr>\n' +
                '         <th class="selectColumn" >选择</th>\n' +
                '         <th>求购内容</th>\n' +
                '         <th>QQ号</th>\n' +
                '         <th>时间</th>\n' +
                '     </tr>\n' +
                '     </thead>\n' +
                '     <tbody>\n' +
                '     </tbody>\n' +
                '</table>');

            var tb = $('#dataGridTableJson tbody');

            // 先清除内容
            $(tb).empty();

            console.log(returnData);

            // 有数据库时填充数据库
            if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                // 显示分页组件
                $('#dataGridTableJson').removeClass("hidden");
                $("#page").removeClass("hidden");

                // 填充数据
                $.each(returnData.data.items, function(i,row){
                    var tr = $('<tr>');
                    $(tr).append('<td align="center"><input type="checkbox" data-id="'+row.id+'"></td>');
                    $(tr).append('<td>'+row.content+'</td>');/*求购内容*/
                    $(tr).append('<td>'+row.user.qq+'</td>');/*qq号*/
                    $(tr).append('<td>'+timestampToTime(row.demandTime)+'</td>');/*时间*/
                    $(tb).append(tr);
                });
            } else { // 没有数据显示提示
            }
        }
    });

    // 添加按钮事件
    // 全部选中
    $('#checkall').click(function () {
        var checked = !($('#checkall').data('checked')==='true');

        //查找tbody中所有第一个td下的input
        var chbs=document.querySelectorAll(
            "tbody td:first-child>input"
        );
        //遍历chbs中每个chb
        for(var i=0;i<chbs.length;i++){
            //设置当前chb的checked等于this的checked
            chbs[i].checked=checked;
        }
        $('#checkall').data('checked',checked.toString());
    });
});

// 批量删除
$('#dels').click(function () {

    //查找tbody中所有第一个td下的input
    var chbs=document.querySelectorAll(
        "tbody td:first-child>input"
    );

    // 存放待删除id
    var ids = [];

    //遍历chbs中每个chb
    for(var i=0;i<chbs.length;i++){
        if (chbs[i].checked===true){
            ids.push($(chbs[i]).data('id'))
        }
    }

    $.ajax({
       url : '/patentQuery/demand/deleteIds',
       type: 'POST',
       cache: false,
       dataType: 'json',
       timeout : 50000, //超时时间：50秒
       async : false,
       data: {ids : ids},
       success : function (returnData) {
           // 刷新当前页
           location.reload();
       }
    });
});

var demand_interval;

/**
 * 文件上传
 */
function fileUpload() {
    // 判断是否选择了文件
    if ($('#excelFile').val() === "") {
        alert("请先选择文件");
        return false;
    }

    $("#importForm").ajaxSubmit({
        url : "/patentQuery/demand/import",//这里写你的url
        type : 'POST',
        datatype:'json',//这里是返回类型，一般是json,text
        resetForm : true,
        success : function(returnData) {
            // 获取进度
            if (returnData.data && returnData.data.taskId) {

                // 1. 打开进度窗口
                layer.open({
                    type: 1,
                    title: '导入进度',
                    skin: 'layui-layer-rim', //加上边框
                    area: ['320px', '180px'], //宽高
                    content:
                    '<div style="padding: 10px; line-height: 34px;">' +
                    '    <div id="progress_wrap" style="width:100%;margin-top:24px;">' +
                    '        <div class="progress">' +
                    '            <div id="progress" class="progress-bar bg-blue" style="width: 0%;">进度：0%</div>' +
                    '        </div>' +
                    '    </div>' +
                    '</div>' +
                    '<button type="button" id="okBtn" class="button border-blue" style="margin-top: 8px; margin-left: 110px;" disabled><span class="icon-check"></span> 确定</button>',
                    cancel: function () {
                        clearInterval(demand_interval);
                        $('#message').html('');
                    }
                });

                $('#progress_wrap').data('taskId', returnData.data.taskId);

                // 2. Ajax 轮询进度——"定时的通过Ajax查询服务端"
                demand_interval = setInterval(function () {
                    $.ajax({
                        url: '/patentQuery/demand/getProgress',
                        type: 'POST',
                        data: {
                            taskId: $('#progress_wrap').data('taskId')
                        },
                        dataType: 'json',
                        success: function (returnData) {
                            if (returnData.data && returnData.data.progress) {
                                var $progress = $('#progress');
                                $progress.text('进度:' + returnData.data.progress + '%');
                                // 并存储到浏览器端
                                sessionStorage.setItem(returnData.data, returnData.data.progress);
                                $progress.width(returnData.data.progress + '%');
                                if (returnData.data.progress === '100.0') {
                                    clearInterval(demand_interval);
                                    sessionStorage.clear();

                                    // 启用按钮
                                    $okBtn = $('#okBtn');
                                    $okBtn.attr('disabled',false);
                                    $okBtn.click(function () {
                                        // 错误记录信息
                                        $('.layui-layer-content').html('<div>' + returnData.data.message +
                                            ' <a href="/patentQuery/demand/download?fileName='+returnData.data.fileName+'">错误记录</a>' +
                                            '</div>');
                                    });
                                }
                            }
                        },
                        error: function (returnData) {
                            console.log('获取进度信息失败');
                        }
                    });
                }, 1000);
            }

            // 刷新整个页面不行
            // window.history.go(0);

            // 重新分页请求
            $('#page').bPage({
                url: '/patentQuery/demand/getList',
                //开启异步处理模式
                asyncLoad: true,
                //关闭服务端页面模式
                serverSidePage: false,
                //数据自定义填充
                render: function (returnData) {
                    // table 容器
                    $('#demandShow').html('' +
                        '<table id="dataGridTableJson" class="hidden table table-striped table-bordered table-hover table-condensed">\n' +
                        '     <thead>\n' +
                        '     <tr>\n' +
                        '         <th class="selectColumn" >选择</th>\n' +
                        '         <th>求购内容</th>\n' +
                        '         <th>QQ号</th>\n' +
                        '         <th>时间</th>\n' +
                        '     </tr>\n' +
                        '     </thead>\n' +
                        '     <tbody>\n' +
                        '     </tbody>\n' +
                        '</table>');

                    var tb = $('#dataGridTableJson tbody');

                    // 先清除内容
                    $(tb).empty();

                    console.log(returnData);

                    // 有数据库时填充数据库
                    if(returnData && returnData.data && returnData.data.items && returnData.data.items.length > 0){
                        // 显示分页组件
                        $('#dataGridTableJson').removeClass("hidden");
                        $("#page").removeClass("hidden");

                        // 填充数据
                        $.each(returnData.data.items, function(i,row){
                            var tr = $('<tr>');
                            $(tr).append('<td align="center"><input type="checkbox" data-id="'+row.id+'"></td>');
                            $(tr).append('<td>'+row.content+'</td>');/*求购内容*/
                            $(tr).append('<td>'+row.user.qq+'</td>');/*qq号*/
                            $(tr).append('<td>'+timestampToTime(row.demandTime)+'</td>');/*时间*/
                            $(tb).append(tr);
                        });
                    } else { // 没有数据显示提示
                    }
                }
            });
        } ,
        error:function(data){
            alert("页面请求失败！");
        }
    });
    return false; //最好返回false，因为如果按钮类型是submit,则表单自己又会提交一次;返回false阻止表单再次提交
}
function indexToPatentType(index) {
    var type;
    switch (index) {
        case 1: type = "发明"; break;
        case 2: type = "实用新型"; break;
        case 3: type = "外观设计"; break;
    }
    return type;
}

function indexToPatentStatus(index) {
    var status;
    switch (index) {
        case 1: status = "未缴费"; break;
        case 2: status = "已下证"; break;
    }
    return status;
}

/**
 * 处理消息码
 * @param returnData
 */
function handleMessageCode(returnData) {
    switch (returnData.messageCode) {
        case "001":
        case "110":
            return true;
        case "002":
        case "101":
        case "102":
        case "103":
        case "104":
        case "105":
        case "106":
        case "107":
        case "108":
        case "109":
        case "111":
            alert(returnData.message);
            return false;
    }
}

/**
 * 获取用户的联系号码
 */
function getAssociationOfUser(user) {
    var association = "";
    if (user.qq !== null && user.qq !== "undefined") {
        association += user.qq;
    }
    if (association !== "") {
        association += "/";
    }
    if (user.telephone !== null && user.telephone !== "undefined") {
        association += user.telephone;
    }else {
        association = association.replace("/","");
    }
    return association;
}

function getFileName(obj){
    var fileName="";

    if(typeof(fileName) != "undefined")
    {
        fileName = $(obj).val().split("\\").pop();
        // fileName=fileName.substring(0, fileName.lastIndexOf("."));
    }
    return fileName;
}

function selectFile(){
    var file = $("#excelFile");

    // 文件提示信息
    file.on('change', function( e ){
        $('#message').html(getFileName('#excelFile'));
    });

    file.trigger("click");
}

// 特效
$('.top_login__link').on({
    mouseenter : function () {
        $('.popup_user').show(500);
    },
    mouseleave :function () {
        setTimeout(function(){
            $('.popup_user').hide(1000);
        },1500);
    }
});

// 获取URL参数
function getUrlParam(paraName) {
    var url = document.location.toString();
    var arrObj = url.split("?");

    if (arrObj.length > 1) {
        var arrPara = arrObj[1].split("&");
        var arr;

        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");

            if (arr != null && arr[0] === paraName) {
                return arr[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
}
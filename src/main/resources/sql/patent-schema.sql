-- 所有外键约束在应用层处理，避免级联风暴
-- 应用层由连接查询需要时，建表中连接的外键，要建立索引
USE `patent`;

DROP TABLE IF EXISTS `sys_admin`;
DROP TABLE IF EXISTS `sys_user`;
DROP TABLE IF EXISTS `sys_resource`;
DROP TABLE IF EXISTS `sys_role`;
DROP TABLE IF EXISTS `sys_user_role`;
DROP TABLE IF EXISTS `sys_role_resource`;
DROP TABLE IF EXISTS `sys_session`;
DROP TABLE IF EXISTS `patent_demand`;
DROP TABLE IF EXISTS `patent_supply`;
DROP TABLE IF EXISTS `patent_sale`;
DROP TABLE IF EXISTS `sys_patent`;
DROP TABLE IF EXISTS `patent_demand_query`;
DROP TABLE IF EXISTS `patent_supply_query`;


CREATE TABLE `sys_admin` (
  `id`                             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`                           VARCHAR(100),
  `password`                       VARCHAR(100),
  `email`                          VARCHAR(100),
  `telephone`                      CHAR(15),
  `qq`                              VARCHAR(100),
  `wechat`                          VARCHAR(100),
  `sex`                             TINYINT,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_sys_admin PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8 ENGINE=InnoDB;

CREATE TABLE `sys_user`(
  `id`                              BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `utype`                           VARCHAR(100), -- utype 目前分为3种：管理员、用户、游客
  `organization_id`                BIGINT,
  `username`                       VARCHAR(100),
  `gender`                         TINYINT UNSIGNED DEFAULT 0, -- 1 为男，0 为女
  `email`                           VARCHAR(100),
  `qq`                              VARCHAR(100),
  `avatar`                         VARCHAR(200), -- 用户头像
  `telephone`                      CHAR(15),
  `wechat`                         VARCHAR(100),
  `password`                       VARCHAR(100),
  `salt`                            VARCHAR(100),
  `is_locked`                      TINYINT UNSIGNED DEFAULT 0,
  `is_validated`                   TINYINT UNSIGNED DEFAULT 1,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_sys_user PRIMARY KEY(`id`)
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;
CREATE UNIQUE INDEX uk_sys_user_username ON sys_user(`username`);
CREATE UNIQUE INDEX uk_sys_user_email ON sys_user(`email`);
CREATE INDEX idx_sys_user_organization_id ON sys_user(`organization_id`);

CREATE TABLE IF NOT EXISTS `sys_resource` (
  `id`                   BIGINT NOT NULL AUTO_INCREMENT,
  `name`                 VARCHAR(100),
  `type`                 VARCHAR(50),
  `url`                  VARCHAR(200),
  `parent_id`           BIGINT,
  `parent_ids`          VARCHAR(100),
  `permission`          VARCHAR(100),
  `is_available`        TINYINT UNSIGNED DEFAULT 1,
  CONSTRAINT pk_sys_resource PRIMARY KEY(`id`)
) DEFAULT CHARSET =utf8 ENGINE=InnoDB;
CREATE INDEX idx_sys_resource_parent_id ON sys_resource(`parent_id`);

CREATE TABLE IF NOT EXISTS `sys_role` (
  `id`                    BIGINT NOT NULL AUTO_INCREMENT,
  `role`                  VARCHAR(100),
  `description`          VARCHAR(100),
  `resource_ids`         VARCHAR(100),
  `is_available`         TINYINT UNSIGNED DEFAULT 1,
  `create_time`          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `update_time`          TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT pk_sys_role PRIMARY KEY(`id`)
) DEFAULT CHARSET =utf8 ENGINE=InnoDB;
CREATE UNIQUE INDEX idx_sys_role_role ON sys_role(`role`);

CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `id`                    BIGINT,
  `user_id`              BIGINT,
  `role_id`              BIGINT,
  CONSTRAINT pk_sys_user_role PRIMARY KEY (`id`)/*,
  CONSTRAINT fk_sys_user_role_user_id FOREIGN KEY (`user_id`) REFERENCES `sys_user`(`id`) ON  DELETE CASCADE, -- 删除用户，用户-角色映射也会删除
  CONSTRAINT fk_sys_user_role_role_id FOREIGN KEY (`role_id`) REFERENCES `sys_role`(`id`) ON  DELETE CASCADE  -- 删除角色，用户-角色映射也会删除*/
) DEFAULT CHARSET =utf8 ENGINE =innoDB;
CREATE INDEX idx_sys_user_role_user_id ON sys_user_role(`user_id`);
CREATE INDEX idx_sys_user_role_role_id ON sys_user_role(`role_id`);

CREATE TABLE IF NOT EXISTS `sys_role_resource` (
  `id`                    BIGINT,
  `role_id`               BIGINT,
  `resource_id`          BIGINT,
  CONSTRAINT pk_sys_role_resource PRIMARY KEY (`id`)/*,
  CONSTRAINT fk_sys_role_resource_role_id FOREIGN KEY (`role_id`) REFERENCES `sys_role`(`id`),
  CONSTRAINT fk_sys_role_resource_resource_id FOREIGN KEY (`resource_id`) REFERENCES `sys_resource`(`id`)*/
) DEFAULT CHARSET =utf8 ENGINE =innoDB;
CREATE INDEX idx_sys_role_resource_role_id ON sys_role_resource(`role_id`);
CREATE INDEX idx_sys_role_resource_resource_id ON sys_role_resource(`resource_id`);

CREATE TABLE IF NOT EXISTS `sys_session` (
  `id`                     BIGINT NOT NULL AUTO_INCREMENT,
  `user_id`               BIGINT,
  `session`               CHAR(36),
  `access_token`         CHAR(36),
  `open_id`               CHAR(32),
  `expire_in`             BIGINT,
  `is_validated`          TINYINT UNSIGNED DEFAULT 1, -- 从QQ登录的session是合法的
  `gmt_create`            DATETIME,
  `gmt_modified`          DATETIME,
  CONSTRAINT pk_sys_session PRIMARY KEY (`id`)
) DEFAULT CHARSET =utf8 ENGINE =InnoDB;

CREATE TABLE `sys_patent`(
  `id`                              CHAR(13) NOT NULL,
  `name`                            VARCHAR(1000),
  `type`                            TINYINT UNSIGNED,
  `application_num`               VARCHAR(20),
  `application_date`              DATETIME,
  `publication_num`               VARCHAR(20),
  `publication_date`              DATETIME,
  `ipc`                            VARCHAR(3000),
  `applicants`                    VARCHAR(2000),
  `applicants_institute`         VARCHAR(45),
  `inventors`                     VARCHAR(2000),
  `priority_num`                  VARCHAR(2000),
  `priority_date`                 VARCHAR(2000),
  `agents`                         VARCHAR(2000),
  `abstract_info`                 VARCHAR(2000),
  `pdf_address`                   VARCHAR(200),
  `law_status`                    TINYINT UNSIGNED,
  `logo`                           VARCHAR(300),
  `gmt_create`                    DATETIME,
  `gmt_modified`                  DATETIME,
  CONSTRAINT pk_sys_patent PRIMARY KEY(`id`)
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;

CREATE TABLE `patent_supply`(
  `id`                             BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `user_id`                       BIGINT UNSIGNED,
  `patent_id`                     CHAR(13),
  `supply_time`                   DATETIME,
  `gmt_create`                    DATETIME,
  `gmt_modified`                  DATETIME,
  CONSTRAINT pk_patent_supply PRIMARY KEY(`id`)/*,
  CONSTRAINT fk_patent_supply_user_id FOREIGN KEY(`user_id`) REFERENCES `sys_user`(`id`),
  CONSTRAINT fk_patent_supply_patent_id FOREIGN KEY(`patent_id`) REFERENCES `sys_patent`(`id`)*/
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;
CREATE INDEX idx_patent_supply_user_id ON patent_supply(`user_id`);
CREATE INDEX idx_patent_supply_patent_id ON patent_supply(`patent_id`);

CREATE TABLE `patent_demand`(
  `id`                              BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `content`                        VARCHAR(256),
  `user_id`                        BIGINT UNSIGNED,
  `demand_time`                    DATETIME,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_patent_demand PRIMARY KEY(`id`)
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;

CREATE TABLE `patent_sale`(
  `id`                              BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `supply_id`                      BIGINT UNSIGNED,
  `demand_id`                      BIGINT UNSIGNED,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_patent_sale PRIMARY KEY(`id`)/*,
  CONSTRAINT fk_patent_sale_supply_id FOREIGN KEY(`supply_id`) REFERENCES `patent_supply`(`id`),
  CONSTRAINT fk_patent_sale_demand_id FOREIGN KEY(`demand_id`) REFERENCES `patent_demand`(`id`)*/
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;
CREATE INDEX idx_patent_sale_supply_id ON patent_sale(`supply_id`);
CREATE INDEX idx_patent_sale_demand_id ON patent_sale(`demand_id`);

CREATE TABLE `patent_demand_query`(
  `id`                              BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `user_id`                        BIGINT UNSIGNED,
  `patent_id`                      CHAR(14),
  `name`                            VARCHAR(256),
  `query_time`                     DATETIME,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_patent_demand_query PRIMARY KEY(`id`)/*,
  CONSTRAINT fk_patent_demand_query_sys_user FOREIGN KEY(`user_id`) REFERENCES `sys_user`(`id`),
  CONSTRAINT fk_patent_demand_query_sys_patent FOREIGN KEY(`patent_id`) REFERENCES `sys_patent`(`id`)*/
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;

CREATE TABLE `patent_supply_query` (
  `id`                              BIGINT UNSIGNED AUTO_INCREMENT NOT NULL,
  `user_id`                        BIGINT UNSIGNED,
  `key`                             VARCHAR(256),
  `query_time`                     DATETIME,
  `gmt_create`                     DATETIME,
  `gmt_modified`                   DATETIME,
  CONSTRAINT pk_patent_supply_query PRIMARY KEY(`id`)/*,
  CONSTRAINT fk_patent_supply_query_sys_user FOREIGN KEY(`user_id`) REFERENCES `sys_user`(`id`)*/
) DEFAULT CHARSET = utf8 ENGINE=InnoDB;